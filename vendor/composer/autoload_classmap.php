<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'AddBackNameInUsersTable' => $baseDir . '/app/database/migrations/2017_02_07_121544_add_back_name_in_users_table.php',
    'AddChangesetInRevisions' => $baseDir . '/app/database/migrations/2017_03_05_141116_add_changeset_in_revisions.php',
    'AddCodeToTenants' => $baseDir . '/app/database/migrations/2018_01_15_170837_add_code_to_tenants.php',
    'AddExtInUploads' => $baseDir . '/app/database/migrations/2017_05_08_064757_add_ext_in_uploads.php',
    'AddHasUplaodsMessagesFromModulesTable' => $baseDir . '/app/database/migrations/2017_05_04_065726_add_has_uplaods_messages_from_modules_table.php',
    'AddIsSystemUserInUsers' => $baseDir . '/app/database/migrations/2017_04_30_081149_add_is_system_user_in_users.php',
    'AddOrderInProductsProdcategoriesUploads' => $baseDir . '/app/database/migrations/2018_01_15_183707_add_order_in_products_prodcategories_uploads.php',
    'AddThemecolorInTenants' => $baseDir . '/app/database/migrations/2018_01_16_081410_add_themecolor_in_tenants.php',
    'AddUploadsMessagesFlagInModules' => $baseDir . '/app/database/migrations/2017_02_15_110028_add_uploads_messages_flag_in_modules.php',
    'AddUserIdInTenants' => $baseDir . '/app/database/migrations/2017_02_04_043813_add_user_id_in_tenants.php',
    'AddUserIdInUserdetails' => $baseDir . '/app/database/migrations/2017_02_05_112500_add_user_id_in_userdetails.php',
    'Addlpermission' => $baseDir . '/app/models/Addlpermission.php',
    'AddlpermissionObserver' => $baseDir . '/app/observers/AddlpermissionObserver.php',
    'AddlpermissionsController' => $baseDir . '/app/controllers/AddlpermissionsController.php',
    'ApiController' => $baseDir . '/app/controllers/ApiController.php',
    'AuthController' => $baseDir . '/app/controllers/AuthController.php',
    'BaseController' => $baseDir . '/app/controllers/BaseController.php',
    'Cartalyst\\Sentry\\Groups\\GroupExistsException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Groups/Exceptions.php',
    'Cartalyst\\Sentry\\Groups\\GroupNotFoundException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Groups/Exceptions.php',
    'Cartalyst\\Sentry\\Groups\\NameRequiredException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Groups/Exceptions.php',
    'Cartalyst\\Sentry\\Throttling\\UserBannedException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Throttling/Exceptions.php',
    'Cartalyst\\Sentry\\Throttling\\UserSuspendedException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Throttling/Exceptions.php',
    'Cartalyst\\Sentry\\Users\\LoginRequiredException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Users/Exceptions.php',
    'Cartalyst\\Sentry\\Users\\PasswordRequiredException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Users/Exceptions.php',
    'Cartalyst\\Sentry\\Users\\UserAlreadyActivatedException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Users/Exceptions.php',
    'Cartalyst\\Sentry\\Users\\UserExistsException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Users/Exceptions.php',
    'Cartalyst\\Sentry\\Users\\UserNotActivatedException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Users/Exceptions.php',
    'Cartalyst\\Sentry\\Users\\UserNotFoundException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Users/Exceptions.php',
    'Cartalyst\\Sentry\\Users\\WrongPasswordException' => $vendorDir . '/cartalyst/sentry/src/Cartalyst/Sentry/Users/Exceptions.php',
    'ChangeGroupFieldsInUsers' => $baseDir . '/app/database/migrations/2017_02_27_073005_change_group_fields_in_users.php',
    'ChangeNameOfParentFields' => $baseDir . '/app/database/migrations/2017_02_16_062143_change_name_of_parent_fields.php',
    'ChangeParentModuleIdToModulegroupId' => $baseDir . '/app/database/migrations/2017_02_15_072631_change_parent_module_id_to_modulegroup_id.php',
    'ChangeToIsEditableByTenant' => $baseDir . '/app/database/migrations/2017_05_07_064000_change_to_is_editable_by_tenant.php',
    'ChangeUploadsElementUuidToString' => $baseDir . '/app/database/migrations/2017_02_27_123018_change_uploads_element_uuid_to_string.php',
    'ChangeUpoloadsElementIdUuid' => $baseDir . '/app/database/migrations/2017_02_27_123705_change_upoloads_element_id_uuid.php',
    'CleanupUsersTable' => $baseDir . '/app/database/migrations/2017_02_05_104909_cleanup_users_table.php',
    'CreateGsettingsTable' => $baseDir . '/app/database/migrations/2017_02_19_104322_create_gsettings_table.php',
    'CreateModulegroupsTable' => $baseDir . '/app/database/migrations/2017_02_15_070556_create_modulegroups_table.php',
    'CreateModulesTable' => $baseDir . '/app/database/migrations/2017_02_08_063428_create_modules_table.php',
    'CreatePermissioncategoriesTable' => $baseDir . '/app/database/migrations/2017_02_15_102824_create_permissioncategories_table.php',
    'CreatePermissionsTable' => $baseDir . '/app/database/migrations/2017_02_15_091903_create_permissions_table.php',
    'CreateProdcategoriesTable' => $baseDir . '/app/database/migrations/2018_01_15_162638_create_prodcategories_table.php',
    'CreateProductsTable' => $baseDir . '/app/database/migrations/2018_01_15_162923_create_products_table.php',
    'CreateRevisionsTable' => $baseDir . '/app/database/migrations/2017_03_05_111302_create_revisions_table.php',
    'CreateTenantsTable' => $baseDir . '/app/database/migrations/2017_01_31_070750_create_tenants_table.php',
    'CreateTsettingsTable' => $baseDir . '/app/database/migrations/2018_01_15_165003_create_tsettings_table.php',
    'CreateUploadsTable' => $baseDir . '/app/database/migrations/2017_02_27_104629_create_uploads_table.php',
    'CreateUploadtypesTable' => $baseDir . '/app/database/migrations/2018_01_15_152640_create_uploadtypes_table.php',
    'CreateUserdetailsTable' => $baseDir . '/app/database/migrations/2017_02_05_105135_create_userdetails_table.php',
    'DashboardController' => $baseDir . '/app/controllers/DashboardController.php',
    'DatabaseSeeder' => $baseDir . '/app/database/seeds/DatabaseSeeder.php',
    'Datamatrix' => $vendorDir . '/tecnickcom/tcpdf/include/barcodes/datamatrix.php',
    'DropPermissioncategoriesTable' => $baseDir . '/app/database/migrations/2017_02_24_055053_drop_permissioncategories_table.php',
    'Group' => $baseDir . '/app/models/Group.php',
    'GroupObserver' => $baseDir . '/app/observers/GroupObserver.php',
    'GroupsController' => $baseDir . '/app/controllers/GroupsController.php',
    'Gsetting' => $baseDir . '/app/models/Gsetting.php',
    'GsettingObserver' => $baseDir . '/app/observers/GsettingObserver.php',
    'GsettingsController' => $baseDir . '/app/controllers/GsettingsController.php',
    'HomeController' => $baseDir . '/app/controllers/HomeController.php',
    'IlluminateQueueClosure' => $vendorDir . '/laravel/framework/src/Illuminate/Queue/IlluminateQueueClosure.php',
    'Maatwebsite\\Excel\\Classes\\Cache' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Classes/Cache.php',
    'Maatwebsite\\Excel\\Classes\\FormatIdentifier' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Classes/FormatIdentifier.php',
    'Maatwebsite\\Excel\\Classes\\LaravelExcelWorksheet' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Classes/LaravelExcelWorksheet.php',
    'Maatwebsite\\Excel\\Classes\\PHPExcel' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Classes/PHPExcel.php',
    'Maatwebsite\\Excel\\Collections\\CellCollection' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Collections/CellCollection.php',
    'Maatwebsite\\Excel\\Collections\\ExcelCollection' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Collections/ExcelCollection.php',
    'Maatwebsite\\Excel\\Collections\\RowCollection' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Collections/RowCollection.php',
    'Maatwebsite\\Excel\\Collections\\SheetCollection' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Collections/SheetCollection.php',
    'Maatwebsite\\Excel\\Excel' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Excel.php',
    'Maatwebsite\\Excel\\ExcelServiceProvider' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/ExcelServiceProvider.php',
    'Maatwebsite\\Excel\\Exceptions\\LaravelExcelException' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Exceptions/LaravelExcelException.php',
    'Maatwebsite\\Excel\\Facades\\Excel' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Facades/Excel.php',
    'Maatwebsite\\Excel\\Files\\ExcelFile' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Files/ExcelFile.php',
    'Maatwebsite\\Excel\\Files\\ExportHandler' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Files/ExportHandler.php',
    'Maatwebsite\\Excel\\Files\\File' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Files/File.php',
    'Maatwebsite\\Excel\\Files\\ImportHandler' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Files/ImportHandler.php',
    'Maatwebsite\\Excel\\Files\\NewExcelFile' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Files/NewExcelFile.php',
    'Maatwebsite\\Excel\\Filters\\ChunkReadFilter' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Filters/ChunkReadFilter.php',
    'Maatwebsite\\Excel\\Parsers\\CssParser' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Parsers/CssParser.php',
    'Maatwebsite\\Excel\\Parsers\\ExcelParser' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Parsers/ExcelParser.php',
    'Maatwebsite\\Excel\\Parsers\\ViewParser' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Parsers/ViewParser.php',
    'Maatwebsite\\Excel\\Readers\\Batch' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Readers/Batch.php',
    'Maatwebsite\\Excel\\Readers\\ConfigReader' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Readers/ConfigReader.php',
    'Maatwebsite\\Excel\\Readers\\Html' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Readers/HtmlReader.php',
    'Maatwebsite\\Excel\\Readers\\LaravelExcelReader' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Readers/LaravelExcelReader.php',
    'Maatwebsite\\Excel\\Writers\\CellWriter' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Writers/CellWriter.php',
    'Maatwebsite\\Excel\\Writers\\LaravelExcelWriter' => $vendorDir . '/maatwebsite/excel/src/Maatwebsite/Excel/Writers/LaravelExcelWriter.php',
    'MigrationCartalystSentryInstallGroups' => $vendorDir . '/cartalyst/sentry/src/migrations/2012_12_06_225929_migration_cartalyst_sentry_install_groups.php',
    'MigrationCartalystSentryInstallThrottle' => $vendorDir . '/cartalyst/sentry/src/migrations/2012_12_06_225988_migration_cartalyst_sentry_install_throttle.php',
    'MigrationCartalystSentryInstallUsers' => $vendorDir . '/cartalyst/sentry/src/migrations/2012_12_06_225921_migration_cartalyst_sentry_install_users.php',
    'MigrationCartalystSentryInstallUsersGroupsPivot' => $vendorDir . '/cartalyst/sentry/src/migrations/2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot.php',
    'Models\\T18\\User' => $baseDir . '/app/models/T18/User.php',
    'Module' => $baseDir . '/app/models/Module.php',
    'ModuleBaseObserver' => $baseDir . '/app/observers/ModuleBaseObserver.php',
    'ModuleCommand' => $baseDir . '/app/commands/ModuleCommand.php',
    'ModuleObserver' => $baseDir . '/app/observers/ModuleObserver.php',
    'Modulegroup' => $baseDir . '/app/models/Modulegroup.php',
    'ModulegroupObserver' => $baseDir . '/app/observers/ModulegroupObserver.php',
    'ModulegroupsController' => $baseDir . '/app/controllers/ModulegroupsController.php',
    'ModulesController' => $baseDir . '/app/controllers/ModulesController.php',
    'Normalizer' => $vendorDir . '/patchwork/utf8/src/Normalizer.php',
    'PDF417' => $vendorDir . '/tecnickcom/tcpdf/include/barcodes/pdf417.php',
    'PermissioncategoriesController' => $baseDir . '/app/controllers/PermissioncategoriesController.php',
    'Permissioncategory' => $baseDir . '/app/models/Permissioncategory.php',
    'PermissioncategoryObserver' => $baseDir . '/app/observers/PermissioncategoryObserver.php',
    'PopulateModulesTable' => $baseDir . '/app/database/migrations/2017_02_08_180427_populate_modules_table.php',
    'ProdcategoriesController' => $baseDir . '/app/controllers/ProdcategoriesController.php',
    'Prodcategory' => $baseDir . '/app/models/Prodcategory.php',
    'ProdcategoryObserver' => $baseDir . '/app/observers/ProdcategoryObserver.php',
    'Product' => $baseDir . '/app/models/Product.php',
    'ProductObserver' => $baseDir . '/app/observers/ProductObserver.php',
    'ProductsController' => $baseDir . '/app/controllers/ProductsController.php',
    'QRcode' => $vendorDir . '/tecnickcom/tcpdf/include/barcodes/qrcode.php',
    'RedefinePermissioncategoriesInAddlpermissions' => $baseDir . '/app/database/migrations/2017_02_24_055215_redefine_permissioncategories_in_addlpermissions.php',
    'RemoveRevisionsTable' => $baseDir . '/app/database/migrations/2017_03_05_110719_remove_revisions_table.php',
    'RenamePermissionsTableToAddlpermissions' => $baseDir . '/app/database/migrations/2017_02_24_054339_rename_permissions_table_to_addlpermissions.php',
    'Revision' => $baseDir . '/app/models/Revision.php',
    'RevisionObserver' => $baseDir . '/app/observers/RevisionObserver.php',
    'RevisionsController' => $baseDir . '/app/controllers/RevisionsController.php',
    'Sentryusergroup' => $baseDir . '/app/classes/Spyrsentrygroup.php',
    'SessionHandlerInterface' => $vendorDir . '/symfony/http-foundation/Resources/stubs/SessionHandlerInterface.php',
    'Spyrdatatable' => $baseDir . '/app/classes/Spyrdatatable.php',
    'SpyrfiySentryUserTable' => $baseDir . '/app/database/migrations/2017_01_31_063821_spyrfiy_sentry_user_table.php',
    'SpyrfySentryGroupsTable' => $baseDir . '/app/database/migrations/2017_02_07_123942_spyrfy_sentry_groups_table.php',
    'Spyrmodule' => $baseDir . '/app/models/SpyrModule.php',
    'SpyrmodulebaseController' => $baseDir . '/app/controllers/SpyrModuleBaseController.php',
    'Spyrsentryuser' => $baseDir . '/app/classes/Spyrsentryuser.php',
    'Spyruser' => $baseDir . '/app/classes/Spyruser.php',
    'SpyruserObserver' => $baseDir . '/app/observers/SpyruserObserver.php',
    'Superhero' => $baseDir . '/app/models/Superhero.php',
    'SuperheroObserver' => $baseDir . '/app/observers/SuperheroObserver.php',
    'SuperheroesController' => $baseDir . '/app/controllers/SuperheroesController.php',
    'TCPDF' => $vendorDir . '/tecnickcom/tcpdf/tcpdf.php',
    'TCPDF2DBarcode' => $vendorDir . '/tecnickcom/tcpdf/tcpdf_barcodes_2d.php',
    'TCPDFBarcode' => $vendorDir . '/tecnickcom/tcpdf/tcpdf_barcodes_1d.php',
    'TCPDF_COLORS' => $vendorDir . '/tecnickcom/tcpdf/include/tcpdf_colors.php',
    'TCPDF_FILTERS' => $vendorDir . '/tecnickcom/tcpdf/include/tcpdf_filters.php',
    'TCPDF_FONTS' => $vendorDir . '/tecnickcom/tcpdf/include/tcpdf_fonts.php',
    'TCPDF_FONT_DATA' => $vendorDir . '/tecnickcom/tcpdf/include/tcpdf_font_data.php',
    'TCPDF_IMAGES' => $vendorDir . '/tecnickcom/tcpdf/include/tcpdf_images.php',
    'TCPDF_IMPORT' => $vendorDir . '/tecnickcom/tcpdf/tcpdf_import.php',
    'TCPDF_PARSER' => $vendorDir . '/tecnickcom/tcpdf/tcpdf_parser.php',
    'TCPDF_STATIC' => $vendorDir . '/tecnickcom/tcpdf/include/tcpdf_static.php',
    'Tenant' => $baseDir . '/app/models/Tenant.php',
    'TenantObserver' => $baseDir . '/app/observers/TenantObserver.php',
    'TenantsController' => $baseDir . '/app/controllers/TenantsController.php',
    'TestCase' => $baseDir . '/app/tests/TestCase.php',
    'Tsetting' => $baseDir . '/app/models/Tsetting.php',
    'TsettingObserver' => $baseDir . '/app/observers/TsettingObserver.php',
    'TsettingsController' => $baseDir . '/app/controllers/TsettingsController.php',
    'UdpateGroupsTable' => $baseDir . '/app/database/migrations/2017_02_20_192658_udpate_groups_table.php',
    'UpdatePermissioncategoriesTable' => $baseDir . '/app/database/migrations/2017_02_15_112618_update_permissioncategories_table.php',
    'Upload' => $baseDir . '/app/models/Upload.php',
    'UploadObserver' => $baseDir . '/app/observers/UploadObserver.php',
    'UploadsController' => $baseDir . '/app/controllers/UploadsController.php',
    'Uploadtype' => $baseDir . '/app/models/Uploadtype.php',
    'UploadtypeObserver' => $baseDir . '/app/observers/UploadtypeObserver.php',
    'UploadtypesController' => $baseDir . '/app/controllers/UploadtypesController.php',
    'User' => $baseDir . '/app/models/User.php',
    'UserObserver' => $baseDir . '/app/observers/UserObserver.php',
    'Userdetail' => $baseDir . '/app/models/Userdetail.php',
    'UserdetailObserver' => $baseDir . '/app/observers/UserdetailObserver.php',
    'UsersController' => $baseDir . '/app/controllers/UsersController.php',
    'Whoops\\Module' => $vendorDir . '/filp/whoops/src/deprecated/Zend/Module.php',
    'Whoops\\Provider\\Zend\\ExceptionStrategy' => $vendorDir . '/filp/whoops/src/deprecated/Zend/ExceptionStrategy.php',
    'Whoops\\Provider\\Zend\\RouteNotFoundStrategy' => $vendorDir . '/filp/whoops/src/deprecated/Zend/RouteNotFoundStrategy.php',
);
