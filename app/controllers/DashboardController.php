<?php

class DashboardController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default dashboard controller
    |--------------------------------------------------------------------------
    |
    | This controller generates dashboards for different type of users related functionalities
    |
    */

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show() {
        if (!Sentry::check()) {
            return Redirect::route('get.signin');
        } else if (userTenantId()) {
            return Redirect::route('tenants.edit', userTenantId());
        } else {
            return Redirect::route('tenants.index');
            //return View::make('spyr.dashboards.default');
        }
    }

}
