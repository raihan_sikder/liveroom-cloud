<?php

class TenantsController extends SpyrmodulebaseController
{
    /*
     * constructor
     */

    public function __construct()
    {
        $this->module_name = controllerModule(get_class());

        /*********************************************
         *  Query extender for grid
         *********************************************/
        // Grid datatable configurations
        $this->db_table = dbTable($this->module_name);
        // Join query
        $this->grid_query = DB::table(DB::raw($this->db_table))
            ->leftJoin('users as updater', DB::raw($this->db_table . '.updated_by'), ' = ', DB::raw('updater.id'))
            ->select(
                DB::raw($this->db_table . '.id as id'),
                DB::raw($this->db_table . '.name as name'),
                DB::raw('updater.name as user_name'),
                DB::raw($this->db_table . '.updated_at as updated_at'),
                DB::raw($this->db_table . '.is_active as is_active')
            )->whereNull(DB::raw($this->db_table . '.deleted_at'));

        // Columns to show 'prefix_table.field','renamed_field','Grid_column_title'
        $this->grid_columns = ['Id', 'Name', 'Updater', 'Update time', 'Active'];
        /**********************************************/

        parent::__construct($this->module_name);
    }

    /**
     * Returns datatable json for the module index page
     * A route is automatically created for all modules to access this controller function
     * @return mixed
     */
    public function grid() {
        // Grid query builder
        $q = $this->grid_query->whereNull($this->module_name . '.deleted_at');

        if(userTenantId())$q = $q->where($this->module_name . '.id',userTenantId());

        // Make datatable
        /** @var Datatables $dt */
        $dt = Spyrdatatable::of($q); // $dt refers to data table.
        $dt = $dt->edit_column('name', '<a href="{{ route(\'' . $this->module_name . '.edit\', $id) }}">{{$name}}</a>');
        $dt = $dt->edit_column('id', '<a href="{{ route(\'' . $this->module_name . '.edit\', $id) }}">{{$id}}</a>');

        return $dt->make();
    }

}
