<?php

class ApiController extends BaseController
{
    public function getCustomer($code) {
        $ret = [];
        if ($customer = Tenant::whereCode($code)->first()) {
            $data = [];
            //$data['customer'] = $customer;
            $data['customer']['id'] = $customer->id;
            $data['customer']['uuid'] = $customer->uuid;
            $data['customer']['name'] = $customer->name;
            $data['customer']['code'] = $customer->code;
            $data['customer']['theme_color'] = $customer->theme_color;

            // Get customer logo
            $data['customer']['logo'] = "#";
            if ($upload = Upload::where('element_id', $customer->id)->where('module_id', $customer->module()->id)->where('uploadtype_id', 1)->whereIsActive('Yes')->first()) {
                $data['customer']['logo'] = $upload->downloadUrl(false);
            }

            // Get customer_start_screen
            $data['customer']['customer_start_screen'] = "#";
            if ($upload = Upload::where('element_id', $customer->id)->where('module_id', $customer->module()->id)->where('uploadtype_id', 5)->whereIsActive('Yes')->first()) {
                $data['customer']['customer_start_screen'] = $upload->downloadUrl(false);
            }

            // Get customer_help_slides
            $data['customer']['customer_help_slides'] = [];
            if ($uploads = Upload::where('element_id', $customer->id)->where('module_id', $customer->module()->id)->where('uploadtype_id', 6)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                foreach ($uploads as $upload) {
                    $data['customer']['customer_help_slides'][] = $upload->downloadUrl(false);
                }
            }

            // Get customer_help_slides
            $data['customer']['customer_3d_files'] = [];
            if ($uploads = Upload::where('element_id', $customer->id)->where('module_id', $customer->module()->id)->where('uploadtype_id', 4)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                foreach ($uploads as $upload) {
                    $data['customer']['customer_help_slides'][] = $upload->downloadUrl(false);
                }
            }

            // Get prodcategories


            $data['customer']['prodcategories'] = Prodcategory::getCategoryTreeForApi($customer->id);


            /*
            if ($prodcategories = Prodcategory::where('tenant_id', $customer->id)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                foreach ($prodcategories as $prodcategory) {

                    // Get category thumbnail
                    $category_thumbnail = '#';
                    if ($upload = Upload::where('element_id', $prodcategory->id)->where('module_id', $prodcategory->module()->id)->where('uploadtype_id', 2)->whereIsActive('Yes')->orderBy('order', 'ASC')->first()) {
                        $category_thumbnail = $upload->downloadUrl(false);
                    }

                    // Get category thumbnail
                    $category_three_D_files = [];
                    if ($uploads = Upload::where('element_id', $prodcategory->id)->where('module_id', $prodcategory->module()->id)->where('uploadtype_id', 4)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                        foreach ($uploads as $upload) {
                            $category_three_D_files[] = $upload->downloadUrl(false);
                        }
                    }

                    // Get category products
                    $category_products = [];
                    if ($products = Product::where('prodcategory_id', $prodcategory->id)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                        foreach ($products as $product) {

                            // Get category thumbnail
                            $product_thumbnail = '#';
                            if ($upload = Upload::where('element_id', $product->id)->where('module_id', $product->module()->id)->where('uploadtype_id', 3)->whereIsActive('Yes')->orderBy('order', 'ASC')->first()) {
                                $product_thumbnail = $upload->downloadUrl(false);
                            }

                            // Get category thumbnail
                            $product_three_D_files = [];
                            if ($uploads = Upload::where('element_id', $product->id)->where('module_id', $product->module()->id)->where('uploadtype_id', 4)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                                foreach ($uploads as $upload) {
                                    $product_three_D_files[] = $upload->downloadUrl(false);
                                }
                            }

                            $category_products[] = [
                                'id' => $product->id,
                                'uuid' => $product->uuid,
                                'name' => $product->name,
                                'code' => $product->code,
                                'order' => (int)$product->order,
                                'price' => (float)$product->price,
                                'desc' => $product->desc,
                                'prodcategory_id' => $product->prodcategory_id,
                                'placement' => $product->placement,
                                'default_height' => $product->default_height,
                                'buy_now_url' => $product->buy_now_url,
                                'rating' => (int)$product->rating,
                                'color_variants' => json_decode($product->color_variants),
                                'product_thumbnail' => $product_thumbnail,
                                'product_3d_files' => $product_three_D_files,
                            ];
                        }
                    }

                    $data['customer']['prodcategories'][] = [
                        'id' => $prodcategory->id,
                        'uuid' => $prodcategory->uuid,
                        'name' => $prodcategory->name,
                        'order' => $prodcategory->order,
                        'parent_id' => $prodcategory->parent_id,
                        'desc' => $prodcategory->desc,
                        'category_thumbnail' => $category_thumbnail,
                        'category_3d_files' => $category_three_D_files,
                        'products' => $category_products,
                    ];
                }
            }
            */
            $ret = ['status'=>'success', 'data' => $data];
        } else {
            $ret = ['status'=>'fail','message' => 'Customer not found'];
        }

        return Response::json($ret);
    }
}