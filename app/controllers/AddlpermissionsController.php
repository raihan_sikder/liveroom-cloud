<?php

class AddlpermissionsController extends SpyrmodulebaseController
{
	/*
	 * constructor
	 */

	public function __construct()
	{
        $this->module_name = controllerModule(get_class());

		/*********************************************
		 *  Query extender for grid
		 *********************************************/
		// Grid datatable configurations
		$this->db_table = dbTable($this->module_name);
		// Join query
		$this->grid_query = DB::table(DB::raw($this->db_table))
			->leftJoin('users as updater', DB::raw($this->db_table . '.updated_by'), ' = ', DB::raw('updater.id'))
			->select(
				DB::raw($this->db_table . '.id as id'),
				DB::raw($this->db_table . '.name as name'),
				DB::raw('updater.name as user_name'),
				DB::raw($this->db_table . '.updated_at as updated_at'),
				DB::raw($this->db_table . '.is_active as is_active')
			)->whereNull(DB::raw($this->db_table . '.deleted_at'));

		// Columns to show 'prefix_table.field','renamed_field','Grid_column_title'
		$this->grid_columns = ['Id', 'Name', 'Updater', 'Update time', 'Active'];
		/**********************************************/

		parent::__construct($this->module_name);
	}


	/**
	 * todo: unused function
	 * this function was initially written to fill up the permissions table
	 */
	public function seedModulePermissions()
	{
		$modules = Module::whereIsActive('Yes')->get();
		$suffixes = [
			'access'       => 'access',
			'view-list'    => 'view grid',
			'view-details' => 'view details',
			'create'       => 'create',
			'edit'         => 'edit',
			'delete'       => 'delete',
			'restore'      => 'restore',
			'change-logs'  => 'change logs',
		];
		foreach ($modules as $module) {

			if ($module->has_uploads) {
				$suffixes ['files-access'] = 'file access';
				$suffixes ['files-view-list'] = 'view file list';
				$suffixes ['files-view-details'] = 'view file details';
				$suffixes ['files-create'] = 'file upload';
				$suffixes ['files-edit'] = 'file edit';
				$suffixes ['files-delete'] = 'file delete';
				$suffixes ['files-download'] = 'file download';
			}
			if ($module->has_messages) {
				$suffixes ['messages-access'] = 'message access';
				$suffixes ['messages-view-list'] = 'view message list';
				//$suffixes ['messages-view-details'] = 'file details';
				$suffixes ['messages-create'] = 'message create';
				$suffixes ['messages-edit'] = 'message edit';
				$suffixes ['messages-delete'] = 'message delete';
			}

			foreach ($suffixes as $suffix_k => $suffix_v) {
				$permission = Permission::firstOrCreate(
					[
						'name'                  => 'perm-module-' . $module->name . "-" . $suffix_k,
						'title'                 => $module->title . " " . $suffix_v,
						'desc'                  => '',
						'parent_id'             => 0,
						'permissioncategory_id' => 1,
						'moudule_id'            => $module->id,
						'moudulegroup_id'       => $module->moudulegroup_id,
						'route'                 => null,
						'route_filters'         => null,
						'functions'             => null,
						'meta'                  => null,
						'is_active'             => 'Yes',

					]
				);
				echo $permission->name . "<br/>";
			}
		}
	}
}
