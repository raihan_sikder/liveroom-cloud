<?php

class UploadsController extends SpyrmodulebaseController
{
    /*
     * constructor
     */

    public function __construct() {
        $this->module_name = controllerModule(get_class());
        /*********************************************
         *  Query extender for grid
         *********************************************/
        // Grid datatable configurations
        $this->db_table = dbTable($this->module_name);
        // Join query
        $this->grid_query = DB::table(DB::raw($this->db_table))
            ->leftJoin('users as updater', DB::raw($this->db_table . '.updated_by'), ' = ', DB::raw('updater.id'))
            ->select(
                DB::raw($this->db_table . '.id as id'),
                DB::raw($this->db_table . '.name as name'),
                DB::raw('updater.name as user_name'),
                DB::raw($this->db_table . '.updated_at as updated_at'),
                DB::raw($this->db_table . '.is_active as is_active')
            )->whereNull(DB::raw($this->db_table . '.deleted_at'));

        # Handle tenant context for grid query
        //        if ($this->tenant_id) {
        //            $this->grid_query = $this->grid_query->where(DB::raw($this->db_table . '.tenant_id'), $this->tenant_id);
        //        }

        // Columns to show 'prefix_table.field','renamed_field','Grid_column_title'
        $this->grid_columns = ['Id', 'Name', 'Updater', 'Update time', 'Active'];
        /**********************************************/

        parent::__construct($this->module_name);
    }

    /**
     * Stores the image file in server
     * @param string $input_name
     * @return $this|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store($input_name = 'file') {

        $input_name = (Input::has('input_name')) ? Input::get('input_name') : $input_name;
        // init local variables
        $module_name = $this->module_name;
        $Model = model($this->module_name);
        $element = str_singular($module_name);
        // $ret = ret();
        # --------------------------------------------------------
        # Process store while creation
        # --------------------------------------------------------
        if (hasModulePermission($this->module_name, 'create')) { // check module permission
            $$element = new $Model(Input::all());
            // validate
            $validator = Validator::make(Input::all(), $Model::rules($$element), $Model::$custom_validation_messages);

            if ($validator->fails()) {
                $ret = ret('fail', "Validation error(s) on updating $Model.", ['validation_errors' => json_decode($validator->messages(), true)]);
            } else {

                if (Input::hasFile($input_name)) {
                    $uuid = uuid();
                    $file = Input::file($input_name);
                    // $unique_name = $uuid . "." . $file->getClientOriginalExtension();
                    $unique_name = randomString() . "_" . $file->getClientOriginalName();
                    $path = conf('spyr.file-upload-root');

                    // Resolve tenant directory
                    $tenant_id = null;
                    if (tenantUser()) { // Obtain tenant_id from logged in user
                        $tenant_id = userTenantId();
                    } else if (Input::has('tenant_id') && strlen(Input::get('tenant_id'))) { // Obtain tenant_id from input
                        if (Tenant::remember(cacheTime('tenant'))->find(Input::get('tenant_id')))
                            $tenant_id = Input::get('tenant_id');
                    }
                    if ($tenant_id) $path .= $tenant_id . "/";

                    // if the file is image file then send the width and height also. This is useful for
                    // plugins like image cropper.
                    $width = $height = null;
                    if (imageExtension($file->getClientOriginalExtension())) {
                        list($width, $height) = getimagesize($file->getPathname());
                    }

                    if ($upload_success = $file->move(public_path() . $path, $unique_name)) {
                        $$element->uuid = $uuid;
                        $$element->name = $file->getClientOriginalName();
                        $$element->path = $path . $unique_name; //save the full path including file to easy retrieve
                        if ($$element->save()) {
                            // data saved in database
                            //$upload = Upload::find($upload->id);
                            $payload = [
                                'data' => $$element,
                                'path' => asset($$element->path),
                                'url' => asset($$element->path),   // required for croppic image cropper
                                "width" => $width,                 // required for croppic image cropper
                                "height" => $height                 // required for croppic image cropper
                            ];
                            $ret = ret('success', "file has been uploaded", $payload);
                        } else {
                            setError("File could not be uploaded for some reason.");
                            $ret = ret('fail', "File could not be uploaded for some reason.");
                        }
                    } else {
                        $ret = ret('fail', "Unable to move file from tmp to destination directory");
                    }
                } else {
                    $ret = ret('fail', "No file selected.");
                }

            }
        } else {
            $ret = ret('fail', "User does not have create permission for module: $Model ");
        }
        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            if ($ret['status'] == 'success' && $ret['redirect'] == '#new') {
                $ret['redirect'] = route("$module_name.edit", $$element->id);
            }
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') {
                $redirect = Redirect::to(Input::get('redirect_fail'))->withInput();
                if (isset($validator)) {
                    $redirect = $redirect->withErrors($validator);
                }
            } else {
                if (Input::get('redirect_success') == '#new') {
                    $redirect = Redirect::route("$module_name.edit", $$element->id);
                } else {
                    $redirect = Redirect::to(Input::get('redirect_success'));
                }
            }
            return $redirect;
        }
    }

    /**
     * Downloads the file with HTTP response to hide the file url
     *
     * @param $uuid
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($uuid) {
        if ($upload = Upload::whereUuid($uuid)->first()) {
            $response = Response::download(public_path() . $upload->path);
            //ob_end_clean();
            return $response;
        }
    }

    /**
     * Used to crop image
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function croppicCrop() {
        $imgUrl = $_POST['imgUrl'];

        // Obtain the upload uuid from the file name
        $ext = pathinfo($imgUrl, PATHINFO_EXTENSION);
        $upload_uuid = basename($imgUrl, "." . $ext);
        $upload = Upload::whereUuid($upload_uuid)->first();

        $jpeg_quality = 80;
        /*
        // original sizes
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        // resized sizes
        $imgW = $_POST['imgW'];
        $imgH = $_POST['imgH'];
        // offsets
        $imgY1 = $_POST['imgY1'];
        $imgX1 = $_POST['imgX1'];
        // crop box
        $cropW = $_POST['cropW'];
        $cropH = $_POST['cropH'];
        */

        // original sizes
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        // resized sizes
        $imgW = $imgInitW;
        $imgH = $imgInitH;
        // offsets

        $ratio = $imgInitW / $_POST['imgW'];

        $imgY1 = $_POST['imgY1'] * $ratio;
        $imgX1 = $_POST['imgX1'] * $ratio;
        // crop box
        $cropW = $_POST['cropW'] * $ratio;
        $cropH = $_POST['cropH'] * $ratio;

        //echo "imgInitW:$imgInitW, imgInitH:$imgInitH, ratio:$ratio, cropW:$cropW,  cropH:$cropH";
        //dd();

        // rotation angle
        $angle = $_POST['rotation'];

        //$output_filename = "temp/croppedImg_" . rand();
        // uncomment line below to save the cropped image in the same location as the original image.

        $destinationPath = conf('spyr.file-upload-root');
        if ($tenant_id = inTenantContext('uploads')) {
            $destinationPath .= $tenant_id . "/";
        }

        $destination_relative_path = $upload->path;
        $filename = "cropped_" . rand();

        /** @var $output_filename Absolute path in server where the file will be stored */
        $output_filename = public_path() . $destination_relative_path;
        $imgUrl = str_replace(' ', "%20", $imgUrl);
        $what = getimagesize($imgUrl);

        switch (strtolower($what['mime'])) {
            case 'image/png':
                $img_r = imagecreatefrompng($imgUrl);
                $source_image = imagecreatefrompng($imgUrl);
                $type = '.png';
                break;
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($imgUrl);
                $source_image = imagecreatefromjpeg($imgUrl);
                error_log("jpg");
                $type = '.jpeg';
                break;
            case 'image/gif':
                $img_r = imagecreatefromgif($imgUrl);
                $source_image = imagecreatefromgif($imgUrl);
                $type = '.gif';
                break;
            default:
                die('image type not supported');
        }

        //Check write Access to Directory
        if (!is_writable(dirname($output_filename))) {
            $response = [
                "status" => 'error',
                "message" => 'Can`t write cropped File',
            ];
        } else {

            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
            // rotate the resized image
            $rotated_image = imagerotate($resizedImage, -$angle, 0);
            // find new width & height of rotated image
            $rotated_width = imagesx($rotated_image);
            $rotated_height = imagesy($rotated_image);
            // diff between rotated & original sizes
            $dx = $rotated_width - $imgW;
            $dy = $rotated_height - $imgH;
            // crop rotated image to fit into original resized rectangle
            $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
            imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
            imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);

            $final_image = imagecreatetruecolor($cropW, $cropH);
            imagecolortransparent($final_image, imagecolorallocate($final_image, 255, 255, 255));
            imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
            // finally output png image
            // imagepng($final_image, $output_filename, $jpeg_quality);

            // Save image and send response
            imagejpeg($final_image, $output_filename, $jpeg_quality);
            $response = [
                "status" => 'success',
                "url" => asset($destination_relative_path),
                "path" => $destination_relative_path,
            ];
        }

        $response = Response::json($response);
        $response->header('Content-Type', 'application/json');

        return $response;
    }
}
