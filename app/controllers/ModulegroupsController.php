<?php

class ModulegroupsController extends SpyrmodulebaseController
{
    /*
     * constructor
     */

    public function __construct()
    {
        $this->module_name = controllerModule(get_class());

        /*********************************************
         *  Query extender for grid
         *********************************************/
        // Grid datatable configurations
        $this->db_table = dbTable($this->module_name);
        // Join query
        $this->grid_query = DB::table(DB::raw($this->db_table))
            ->leftJoin('users as updater', DB::raw($this->db_table . '.updated_by'), ' = ', DB::raw('updater.id'))
            ->leftJoin('modulegroups as parent', DB::raw($this->db_table . '.parent_id'), ' = ', DB::raw('parent.id'))
            ->select(
                DB::raw($this->db_table . '.id as id'),
                DB::raw($this->db_table . '.name as name'),
                DB::raw($this->db_table . '.title as title'),
                DB::raw('parent.title as parent_title'),
                DB::raw($this->db_table . '.`level` as `level`'),
                DB::raw($this->db_table . '.`order` as `order`'),
                DB::raw($this->db_table . '.color_css as color_css'),
                DB::raw($this->db_table . '.icon_css as icon_css'),
                DB::raw($this->db_table . '.route as route'),
                DB::raw('updater.name as user_name'),
                DB::raw($this->db_table . '.updated_at as updated_at'),
                DB::raw($this->db_table . '.is_active as is_active')
            )->whereNull(DB::raw($this->db_table . '.deleted_at'));

        // Columns to show 'prefix_table.field','renamed_field','Grid_column_title'
        $this->grid_columns = ['Id', 'Name', 'Title', 'Parent', 'Level', 'Order', 'Color', 'Icon', 'Route', 'Updater', 'Update time', 'Active'];
        /**********************************************/

        parent::__construct($this->module_name);
    }

    public function modulegroupIndex()
    {

        return Route::getCurrentRoute()->getName();
    }

}
