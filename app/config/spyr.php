<?php
return [
    /*
	|--------------------------------------------------------------------------
	| File root
	|--------------------------------------------------------------------------
	|
	| Supported: Any integer with value greater than zero
	|
	*/
    'file-upload-root'        => '/files/',
    /*
	|--------------------------------------------------------------------------
	| Tenant field name in table
	|--------------------------------------------------------------------------
	|
	| Existence of the following field indicates that this table is a tenant
    | specific table.
	|
	*/
    'tenant_field_identifier' => 'tenant_id',
];
