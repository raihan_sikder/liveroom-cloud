<?php

class ExampleTest extends TestCase
{

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {

        Session::start();
        $response = $this->call('POST', '/auth/signin', [
            'name'     => 'tenant6',
            'password' => 'tenant6',
            'ret'      => 'json',
            '_token'   => csrf_token(),
        ]);

        $ret = json_decode($response->getContent());
        $this->assertEquals('success1', $ret->status);
    }

    /**
     *
     */
    public function testLogin2()
    {
        $this->assertEquals('1', '2');
        $this->assertEquals('success', 'ful');
    }

}
