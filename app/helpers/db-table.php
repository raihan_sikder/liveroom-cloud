<?php


/**
 * Returns the database table column field names in an array. The parameter can be both string or array.
 *
 * @param array|string $table : table name excluding prefix. i.e. 'users'
 * @return array
 */
function columns($table)
{
    $columns = [];
    // Handle parameter as array/non array
    $tables = [];
    if (!is_array($table)) {
        array_push($tables, $table);
    } else {
        $tables = $table;
    }

    /*
    $database = Config::get('database.connections.mysql.database');
    $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$database' AND TABLE_NAME = '" . dbTable($table) . "'";
    */
    foreach ($tables as $table) {
        $sql = "SHOW COLUMNS FROM `" . dbTable($table) . "`";
        $results = result($sql, cacheTime('table-field-list'));
        foreach ($results as $result) {
            array_push($columns, $result->Field);
        }
    }

    return $columns;
}


/**
 * Adds prefix to a table name. This is useful for raw query. Laravel by default does not
 * require table prefix.
 *
 * @param $table : table name without prefix
 * @return string Table name with prefix added
 */
function dbTable($table)
{
    $table_prefix = DB::getTablePrefix();
    // Checks if table name already has prefix.
    if (strpos($table, $table_prefix)) {
        return $table;
    }

    // If no prefix is added then a string is returned by adding a prefix.
    return $table_prefix . $table;
}



/**
 * @param       $Model
 * @param array $except
 * @return array
 * @internal param type $table i.e facilities without table prefix
 */
function getModelFields($Model, $except = [])
{
    return array_diff(columns(modelTable($Model)), $except);
}

/**
 * Checks if a field exists in a table
 *
 * @param string $table
 * @param string $column
 * @return bool
 */
function tableHasColumn($table, $column)
{
    if (in_array($column, columns($table))) return true;
    return false;
}

/**
 * get the table name(with prefix) used by a model
 *
 * @param string $Model
 * @return string
 */
function modelTable($Model)
{
    return DB::getTablePrefix() . str_plural(lcfirst($Model));
}
