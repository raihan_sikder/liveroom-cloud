<?php

/**
 * ceate uuid
 *
 * @return string
 * @throws Exception
 */
function uuid() {
    $uuid = Webpatser\Uuid\Uuid::generate(4);
    return (string)$uuid;
}

/**
 * function to check if an input field has a value predefined that needs to be
 * retained. This function is mostly used in blade view partials of form
 * elements. By default it tries to load the value using Input::old('input_name')
 * However this value is over-ridden if there an explicit GET or POST with
 * that parameter/input name. This value gets further overridden if a
 * value is passed as parameter to the view partial
 *
 * @param string $name
 * @param string|array $value
 * @return mixed
 */
function oldInputValue($name = '', $value = null) {
    // first get anything old
    $val = Input::old($name); // array/non-array both handled
    // override by explicit inputs
    if (Input::has($name)) {
        $val = Input::get($name);
    }
    // override by explicit parameter passed in function
    if (isset($value) && (is_array($value) || strlen(trim($value)))) {
        $val = $value;
    }
    return $val;
}

/**
 * For all models While saving(create or update) we need to fill some common fields, trim the blank spaces from
 * inputs and set the empty strings to null. Also for all cases we need to set the creator/updater/deleter
 * with timestamp of that event.
 * This function is generally used in Model saving() event
 *
 * @param \Illuminate\Database\Eloquent\Model $element Eloquent model object
 * @param array $except If any field should be ignored from auto filling then should be
 *     defined in this array
 * @return mixed : Eloquent model object with filled and cleaned values
 * @throws Exception
 */
function fillModel($element, $except = []) {
    $module_name = moduleName(get_class($element));
    // uuid
    if (!isset($element->uuid)) {
        $element->uuid = Webpatser\Uuid\Uuid::generate(4); // 4 = truly random, uncomment this when you have uuid field added
    }

    // created_by & created_at
    $created_by = 1;
    $created_by = (isset($element->created_by)) ? $element->created_by : $created_by;
    $created_by = (!isset($element->created_by) && Sentry::check()) ? Sentry::getUser()->id : $created_by;
    $created_by = (!isset($element->created_by) && Input::has('created_by')) ? Input::get('created_by') : $created_by;
    $element->created_by = $created_by;
    $element->created_at = (!isset($element->created_at)) ? now() : $element->created_at;

    // updated_by & updated_at
    $updated_by = 1;
    $updated_by = (isset($element->updated_by)) ? $element->updated_by : $updated_by;
    $updated_by = (!isset($element->updated_by) && Sentry::check()) ? Sentry::getUser()->id : $updated_by;
    $updated_by = (!isset($element->created_by) && Input::has('updated_by')) ? Input::get('updated_by') : $updated_by;
    $element->updated_by = $updated_by;
    $element->updated_at = now();

    // fill with null if not array
    $fields = array_diff(columns($module_name), ['id']);
    foreach ($fields as $field) {
        if (isset($element->$field) && !in_array($field, $except) && !is_array($element->$field)) {
            $element->$field = trim($element->$field); // trim white space
            if (!strlen($element->$field)) $element->$field = null;
        }
    }
    // inject tenant context
    if (inTenantContext($module_name)) $element = fillTenantId($element);

    return $element;
}

/**
 * returns the table/module name(without prefix) from a model class name
 *
 * @param  Model $class Class name with first letter in uppercase. i.e. Foo
 * @return string
 */
function moduleName($class) {
    return str_plural(lcfirst(class_basename($class)));
}

/**
 * Returns model name with Uppercase first letter and singular.
 * If there is tenant specific models then full class path is returned
 *
 * @param $module : plural, lowercase first letter
 * @return string
 */
function model($module) {
    return modelClassPath(str_singular(ucfirst($module)));
}

/**
 * Returns controller name with Uppercase first letter and singular.
 *
 * @param $module
 * @return string
 */
function controller($module) {
    return ucfirst($module) . "Controller";
}

/**
 * Derive the module name from controller class name
 *
 * @param $controller_class
 * @return string
 */
function controllerModule($controller_class) {
    return lcfirst(str_replace("Controller", '', class_basename($controller_class)));
}

/**
 * Derive module name from an eloquent model element
 *
 * @param $element
 * @return string
 * @internal param $element_object
 */
function elementModule($element) {
    return moduleName(class_basename(get_class($element)));
}

/**
 * This function pushes an error string to 'error' array of session.
 *
 * @param string $str
 * @param bool $ret
 * @param bool $set_msg
 * @return bool
 */
function setError($str = '', $set_msg = true, $ret = false) {
    if ($set_msg && strlen($str)) Session::push('error', $str);
    return $ret;
}

/**
 * This function pushes an error string to 'message' array of session.
 *
 * @param string $str
 * @param bool $ret
 * @param bool $set_msg
 * @return bool
 */
function setMessage($str = '', $set_msg = true, $ret = true) {
    if ($set_msg && strlen($str)) Session::push('message', $str);
    return $ret;
}

/**
 * This function pushes an error string to 'success' array of session.
 *
 * @param string $str
 * @param bool $ret
 * @param bool $set_msg
 * @return bool
 */
function setSuccess($str = '', $set_msg = true, $ret = true) {
    if ($set_msg && strlen($str)) Session::push('success', $str);
    return $ret;
}

/**
 * @param string $str
 * @param bool $ret
 * @param bool $set_msg
 * @return bool
 */
function setDebug($str = '', $set_msg = true, $ret = true) {
    if ($set_msg && strlen($str)) Session::push('debug', $str);
    return $ret;
}

/**
 * Prepares the return array for Conroller post operations (store, update, delete, restore).
 * This array contains the value that will be returned as json as a consequence of
 * CRUD operation.
 *
 * @param string $status
 * @param string $msg
 * @param array $merge : contains additioanl data that needs to be returned as json
 * @return array
 */
function ret($status = '', $msg = '', $merge = []) {
    if ($status == 'fail') setError($msg);
    else if ($status == 'success') setSuccess($msg);
    else setMessage($msg);

    $ret = [
        'status' => $status,
        'message' => $msg,
        'validation_errors' => [],
    ];

    return array_merge($ret, $merge);
}

/**
 * This function fills ajax return values with saved information from session and then cleans up the session.
 * @param $ret
 * @return mixed
 */
function fillFromSession($ret) {
    $sessionKeys = ['message', 'error', 'success'];
    foreach ($sessionKeys as $k) {
        $ret['session_' . $k] = [];
        if (Session::has($k)) {
            if (is_array(Session::get($k))) {
                $ret['session_' . $k] = Session::get($k); // if array load the whole array
            } else {
                array_push($ret['session_' . $k], Session::get($k)); // if not array load it in an array as single element
            }
        }
        Session::forget($k);
    }

    //if (!Session::has('validation_errors')) $ret['validation_errors'] = [];

    return $ret;
}

/**
 * Fill the $ret variable with redirect and session information.
 * This function is used SpyrmodulebaseController to build the return JSON
 * @param $ret
 * @return mixed
 */
function fillRet($ret) {
    $ret = redirect($ret);
    $ret = fillFromSession($ret);
    return $ret;
}

/**
 * Get the redirect url from input parameter redirect_success, redirect_fail
 *
 * @param $ret
 * @return mixed
 */
function redirect($ret) {
    switch ($ret['status']) {
        case 'success':
            $ret['redirect'] = Input::has('redirect_success') ? Input::get('redirect_success') : '';
            break;
        case 'fail':
            $ret['redirect'] = Input::has('redirect_fail') ? Input::get('redirect_fail') : '';
            break;
        default :
            $ret['redirect'] = '';
    }
    return $ret;
}

/**
 * @param        $route
 * @param string $redirect_success
 * @param string $redirect_fail
 * @param string $class
 * @param string $text
 * @return string
 */
function deleteBtn($route, $redirect_success = '', $redirect_fail = '', $class = 'btn btn-danger flat', $text = 'Delete') {
    return "<button	name='genericDeleteBtn'
            type='button'
            data-toggle='modal'
            data-target='#deleteModal'
            class='$class'
            data-route='$route'
            data-redirect_success='$redirect_success'
            data-redirect_fail='$redirect_fail'>$text</button>";

}

/**
 * Shorthand function for fetching configuration
 *
 * @param string $key
 * @return mixed|null
 */
function conf($key = '') {
    if (Config::has($key)) {
        return Config::get($key);
    }
    return null;
}

/**
 * Function to return the cache time defined in
 *
 * @param $key
 * @return mixed
 */
function cacheTime($key) {
    return Config::get('cache.cache_time.' . $key);
}

/**
 * Spyr framework has settings stored as boolean, string or array(input as json).
 * These settings are stored in Global Settings(gsettings) module. Some of
 * these settings can be over-ridden by tenant. Those tenant specific settings
 * are stored in Tenant Settings(tsettings) module.
 *
 * @param $name
 * @return bool|mixed|string
 */
function setting($name) {
    $val = "";

    // if ($setting = Tsetting::whereName($name)->remember(cacheTime('system-cache-time'))->first(['value', 'type'])) {
    //     $val = $setting->value;
    // } else
    if ($setting = Gsetting::with('uploads')->whereName($name)->remember(cacheTime('system-cache-time'))->first(['value', 'type'])) {
        $val = $setting->value;
    }
    switch ($setting->type) {
        case 'boolean':
            $val = $setting->value == 'true' ? true : false;
            break;
        case 'string':
            $val = $setting->value;
            break;
        case 'array':
            $val = json_decode($setting->value, true);
            break;
    }
    return $val;
}

/**
 * Renders the left menu of the application and makes the current item active based on breadcrumb
 *
 * @param        $tree
 * @param string $current_module_name
 * @param array $breadcrumbs
 */
function renderMenuTree($tree, $current_module_name = '', $breadcrumbs = []) {
    if (is_array($tree)) {
        foreach ($tree as $leaf) {
            $p_name = "perm-" . $leaf['type'] . "-" . $leaf['item']->name;

            if (hasPermission($p_name)) {

                // 1. checks if an item has any children
                $has_children = false;
                if (isset($leaf['children']) && count($leaf['children'])) {
                    $has_children = true;
                }

                // set tree view if there is children
                $li_class = '';
                if ($has_children) $li_class = 'treeview';

                // set url of the item
                $url = '#';
                if (in_array($leaf['type'], ['module', 'modulegroup'])) {
                    $route = $leaf['item']->name . ".index";
                    $url = route($route);
                }

                // matching current breadcrumb of the application set an item as active
                if (array_key_exists($leaf['item']->name, $breadcrumbs)) {
                    $li_class .= " active";
                }

                echo "<li class='$li_class'>";
                echo "<a href=\"$url\"><i class=\"" . $leaf['item']->icon_css . "\"></i><span>" . $leaf['item']->title . "</span> ";
                if ($has_children) {
                    echo "<span class=\"pull-right-container\"> <i class=\"fa fa-angle-left pull-right\"></i> </span> ";
                }
                echo "</a>";

                // for children recursively draw the tree
                if ($has_children) {
                    echo "<ul class=\"treeview-menu\">";
                    renderMenuTree($leaf['children'], $current_module_name, $breadcrumbs);
                    echo "</ul>";
                }
                echo "</li>";
            } else {
                // echo "<li class=''>!  $p_name</li>";
            }
        }
        return;
    }
}

/**
 * Returns an array with module/modulegroup name as key
 *
 * @param Module|null $module
 * @return array
 */
function breadcrumb(Module $module = null) {
    $breadcrumbs = [];
    if ($module) {
        $items = $module->modulegroupTree();
        foreach ($items as $item) {
            $breadcrumbs[$item->name] = [
                'name' => $item->name,
                'title' => $item->title,
                'route' => "$item->name.index",
                'url' => route("$item->name.index"),
            ];
        }
    }
    return $breadcrumbs;
}

/**
 * returns absolute path from a relative path
 *
 * @param $relative_path
 * @return string
 */
function absPath($relative_path) {
    return public_path() . $relative_path;
}

/**
 * Show the default error page
 *
 * @param string $title
 * @param string $body
 * @return $this
 */
function renderStaticPage($title = '', $body = '') {
    return View::make('spyr.template.blank')->with('title', $title)->with('body', $body);
}

/**
 * Show the permission error page
 *
 * @param string $body
 * @return $this
 */
function showPermissionErrorPage($body = '') {
    return renderStaticPage('Permission denied!', $body);
}

/**
 * Show generic error page
 * @param string $body
 * @return $this
 */
function showGenericErrorPage($body = '') {
    return renderStaticPage('Error!', $body);
}
