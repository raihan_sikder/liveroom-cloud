<?php

/**
 * Returns the class path of any tenant specific models.
 * Tenant specific models are stored in Models\TXX\Model.php
 * @param $class_name
 * @return string
 */
function modelClassPath($class_name)
{

    if (tenantUser()) {
        $class_path = "Models\\T" . userTenantId() . "\\$class_name";
        if (class_exists($class_path)) {
            return $class_path;
        }
    }
    return $class_name;
}

/**
 * Function Alias for Model.
 * @return Module
 */
function Module()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for User.
 * @return User
 */
function User()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Userdetail.
 * @return Userdetail
 */
function Userdetail()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Group.
 * @return Group
 */
function Group()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Tenant.
 * @return Tenant
 */
function Tenant()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Addlpermission.
 * @return Addlpermission
 */
function Addlpermission()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Modulegroup.
 * @return Modulegroup
 */
function Modulegroup()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Permissioncategory.
 * @return Permissioncategory
 */
function Permissioncategory()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Gsetting.
 * @return Gsetting
 */
function Gsetting()
{

    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Upload.
 * @return Upload
 */
function Upload()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Revision.
 * @return Revision
 */
function Revision()
{
    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}

/**
 * Function Alias for Model.
 * @return Testmodule
 */
function Testmodule()
{

    $class_path = modelClassPath(__FUNCTION__);
    return new $class_path;
}