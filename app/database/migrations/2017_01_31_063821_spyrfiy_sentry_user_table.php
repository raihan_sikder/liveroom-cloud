<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SpyrfiySentryUserTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('uuid', 36)->nullable()->default(null)->after('id');
            $table->integer('tenant_id')->unsigned()->nullable()->default(null)->after('uuid');
            $table->string('name', 36)->nullable()->default(null)->after('tenant_id');
            $table->string('groups', 512)->nullable()->default(null)->after('permissions');
            $table->string('is_active', 3)->nullable()->default(null)->after('last_name');
            $table->integer('created_by')->unsigned()->nullable()->default(null)->after('created_at');
            $table->integer('updated_by')->unsigned()->nullable()->default(null)->after('updated_at');
            $table->softDeletes();
            $table->integer('deleted_by')->unsigned()->nullable()->default(null)->after('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn(['uuid', 'name', 'tenant_id', 'groups', 'is_active', 'created_by', 'updated_by', 'deleted_at', 'deleted_by']);
        });
    }

}
