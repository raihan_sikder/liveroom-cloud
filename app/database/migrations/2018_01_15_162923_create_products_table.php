<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->integer('tenant_id')->unsigned()->nullable()->default(null);
            $table->string('uuid', 36)->nullable()->default(null);
            $table->string('name', 255)->nullable()->default(null);
            // -----------------------------------------------------
            // Add custom fields
            $table->string('code', 36)->nullable()->default(null);
            $table->double('price')->unsigned()->nullable()->default(null);
            $table->string('desc', 1000)->nullable()->default(null);
            $table->integer('prodcategory_id')->unsigned()->nullable()->default(null);
            $table->string('placement', 10)->nullable()->default(null);
            $table->integer('default_height')->unsigned()->nullable()->default(null);
            $table->string('buy_now_url', 500)->nullable()->default(null);
            $table->smallInteger('rating')->unsigned()->nullable()->default(null);
            $table->string('color_variants', 500)->nullable()->default(null);
            // -----------------------------------------------------
            $table->string('is_active', 3)->nullable()->default(null);
            $table->integer('created_by')->unsigned()->nullable()->default(null);
            $table->integer('updated_by')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('deleted_by')->unsigned()->nullable()->default(null);
        });

        DB::table('modules')->insert(
            [
                'uuid' => Uuid::generate(4),
                'name' => 'products',
                'title' => 'Product',
                'desc' => '',
                'parent_id' => 0,
                'modulegroup_id' => 0,
                'level' => 0,
                'order' => 0,
                'color_css' => 'aqua',
                'icon_css' => 'fa fa-plus',
                'route' => 'products.index',
                'is_active' => 'Yes',
                'created_at' => now(),
                'created_by' => '1',
                'updated_at' => now(),
                'updated_by' => '1'
            ]
        );
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        // drop the module table
        Schema::dropIfExists('products');
        // remove the module entry from modules table
        DB::table('modules')->where('name', 'products')->delete();
    }

}