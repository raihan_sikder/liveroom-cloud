<?php

use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;

class CreateModulegroupsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modulegroups', function ($table) {

			$table->engine = 'InnoDB';

			$table->bigIncrements('id');
			$table->string('uuid', 36)->nullable()->default(null);
			$table->string('name', 255)->nullable()->default(null);

			// custom fields starts
			$table->string('title', 100)->nullable()->default(null);
			$table->string('desc', 255)->nullable()->default(null);
			$table->integer('parent_modulegroup_id')->unsigned()->nullable()->default(null);
			$table->integer('level')->unsigned()->nullable()->default(null);
			$table->integer('order')->unsigned()->nullable()->default(null);
			$table->string('color_css', 128)->nullable()->default(null);
			$table->string('icon_css', 128)->nullable()->default(null);
			$table->string('route', 128)->nullable()->default(null);

			// custom fields ends
			$table->string('is_active', 3)->nullable()->default(null);
			$table->integer('created_by')->unsigned()->nullable()->default(null);
			$table->integer('updated_by')->unsigned()->nullable()->default(null);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable()->default(null);
		});

		DB::table('modules')->insert(
			[
				'uuid'             => Uuid::generate(4),
				'name'             => 'modulegroups',
				'title'            => 'Module group',
				'desc'             => '',
				'parent_module_id' => '0',
				'level'            => 0,
				'order'            => 0,
				'color_css'        => 'aqua',
				'icon_css'         => 'fa fa-plus',
				'route'            => 'modulegroups.index',
				'is_active'        => 'Yes',
				'created_at'       => now(),
				'created_by'       => '1',
				'updated_at'       => now(),
				'updated_by'       => '1'
			]
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// drop the module table
		Schema::dropIfExists('modulegroups');
		// remove the module entry from modules table
		DB::table('modules')->where('name', 'modulegroups')->delete();
	}

}