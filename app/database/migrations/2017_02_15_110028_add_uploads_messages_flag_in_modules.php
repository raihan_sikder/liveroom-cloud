<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadsMessagesFlagInModules extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasColumn('modules', 'modulegroup_id')) {
			Schema::table('modules', function ($table) {
				$table->dropColumn('modulegroup_id');
			});
		}

		Schema::table('modules', function ($table) {
			$table->integer('modulegroup_id')->unsigned()->nullable()->default(null)->after('parent_module_id');
			$table->string('has_uploads', 3)->nullable()->default(null)->after('route');
			$table->string('has_messages', 3)->nullable()->default(null)->after('has_uploads');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasColumn('modules', 'modulegroup_id')) {
			Schema::table('modules', function ($table) {
				$table->dropColumn('modulegroup_id');
			});
		}
		if (Schema::hasColumn('modules', 'has_uploads')) {
			Schema::table('modules', function ($table) {
				$table->dropColumn('has_uploads');
			});
		}
		if (Schema::hasColumn('modules', 'has_messages')) {
			Schema::table('modules', function ($table) {
				$table->dropColumn('has_messages');
			});
		}
	}

}
