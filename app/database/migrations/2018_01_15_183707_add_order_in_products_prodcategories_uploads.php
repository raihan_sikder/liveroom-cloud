<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderInProductsProdcategoriesUploads extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('products', function (Blueprint $table) {
            $table->smallInteger('order')->unsigned()->nullable()->default(null)->after('code');
        });
        Schema::table('prodcategories', function (Blueprint $table) {
            $table->smallInteger('order')->unsigned()->nullable()->default(null)->after('name');
        });
        Schema::table('uploads', function (Blueprint $table) {
            $table->smallInteger('order')->unsigned()->nullable()->default(null)->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
