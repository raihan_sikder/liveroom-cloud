<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtInUploads extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('uploads',function(Blueprint $table){
            $table->string('ext',10)->nullable()->default(null)->after('path');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('uploads',function(Blueprint $table){
            $table->dropIfExists('ext');
        });
	}

}
