<?php

use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;

class CreateRevisionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisions', function ($table) {

            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->integer('tenant_id')->unsigned()->nullable()->default(null);
            $table->string('uuid', 36)->nullable()->default(null);
            $table->string('name', 255)->nullable()->default(null);

            // custom fields starts
            $table->integer('module_id')->unsigned()->nullable()->default(null);
            $table->string('module_name', 100)->nullable()->default(null);
            $table->integer('element_id')->unsigned()->nullable()->default(null);
            $table->string('element_uuid', 36)->nullable()->default(null);
            $table->string('field', 100)->nullable()->default(null);
            $table->text('old')->nullable()->default(null);
            $table->text('new')->nullable()->default(null);
            $table->string('desc', 512)->nullable()->default(null);

            // custom fields ends
            $table->string('is_active', 3)->nullable()->default(null);
            $table->integer('created_by')->unsigned()->nullable()->default(null);
            $table->integer('updated_by')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('deleted_by')->unsigned()->nullable()->default(null);
        });

        DB::table('modules')->insert(
            [
                'uuid'           => Uuid::generate(4),
                'name'           => 'revisions',
                'title'          => 'Revision',
                'desc'           => '',
                'parent_id'      => 0,
                'modulegroup_id' => 0,
                'level'          => 0,
                'order'          => 0,
                'color_css'      => 'aqua',
                'icon_css'       => 'fa fa-plus',
                'route'          => 'revisions.index',
                'is_active'      => 'Yes',
                'created_at'     => now(),
                'created_by'     => '1',
                'updated_at'     => now(),
                'updated_by'     => '1'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop the module table
        Schema::dropIfExists('revisions');
        // remove the module entry from modules table
        DB::table('modules')->where('name', 'revisions')->delete();
    }

}