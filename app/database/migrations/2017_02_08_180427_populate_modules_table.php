<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateModulesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// truncate existing modules table an store some base modules
		DB::table('modules')->truncate();

		Module::create(['name' => 'modules', 'title' => 'Modules', 'desc' => 'Manage modules', 'is_active' => 'Yes']);
		Module::create(['name' => 'users', 'title' => 'Users', 'desc' => 'Manage users', 'is_active' => 'Yes']);
		Module::create(['name' => 'userdetails', 'title' => 'Userdetails', 'desc' => 'Manage user details', 'is_active' => 'Yes']);
		Module::create(['name' => 'groups', 'title' => 'Groups', 'desc' => 'Manage groups', 'is_active' => 'Yes']);
		Module::create(['name' => 'tenants', 'title' => 'Tenants', 'desc' => 'Manage tenants', 'is_active' => 'Yes']);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
