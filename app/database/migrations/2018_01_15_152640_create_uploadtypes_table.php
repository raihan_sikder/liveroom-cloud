<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;

class CreateUploadtypesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::create('uploadtypes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->integer('tenant_id')->unsigned()->nullable()->default(null);
            $table->string('uuid', 36)->nullable()->default(null);
            $table->string('name', 255)->nullable()->default(null);
            $table->string('desc', 1024)->nullable()->default(null);
            // custom fields starts
            $table->string('allowed_exts', 255)->nullable()->default(null);
            $table->integer('max_filesize_bytes')->unsigned()->nullable()->default(null);
            $table->string('fixed_aspect_ratio',10)->nullable()->default(null);
            $table->integer('fixed_img_width_px')->unsigned()->nullable()->default(null);
            $table->integer('fixed_img_height_px')->unsigned()->nullable()->default(null);
            $table->integer('min_img_width_px')->unsigned()->nullable()->default(null);
            $table->integer('min_img_height_px')->unsigned()->nullable()->default(null);
            $table->integer('max_img_width_px')->unsigned()->nullable()->default(null);
            $table->integer('max_img_height_px')->unsigned()->nullable()->default(null);
            // custom fields ends
            $table->string('is_active', 3)->nullable()->default(null);
            $table->integer('created_by')->unsigned()->nullable()->default(null);
            $table->integer('updated_by')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('deleted_by')->unsigned()->nullable()->default(null);
        });

        DB::table('modules')->insert(
            [
                'uuid' => Uuid::generate(4),
                'name' => 'uploadtypes',
                'title' => 'Uploadtype',
                'desc' => '',
                'parent_id' => 0,
                'modulegroup_id' => 0,
                'level' => 0,
                'order' => 0,
                'color_css' => 'aqua',
                'icon_css' => 'fa fa-plus',
                'route' => 'uploadtypes.index',
                'is_active' => 'Yes',
                'created_at' => now(),
                'created_by' => '1',
                'updated_at' => now(),
                'updated_by' => '1'
            ]
        );

        Schema::table('uploads', function (Blueprint $table) {
            $table->integer('uploadtype_id')->unsigned()->nullable()->default(null)->after('ext');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        // drop the module table
        Schema::dropIfExists('uploadtypes');
        // remove the module entry from modules table
        DB::table('modules')->where('name', 'uploadtypes')->delete();
    }

}