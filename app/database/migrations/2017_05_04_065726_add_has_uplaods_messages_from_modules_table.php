<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasUplaodsMessagesFromModulesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('modules', function (Blueprint $table) {

			if (!Schema::hasColumn('modules', 'has_uploads'))
				$table->string('has_uploads', 3)->nullable()->default(null)->after('route');
			if (!Schema::hasColumn('modules', 'has_messages'))
				$table->string('has_messages', 3)->nullable()->default(null)->after('has_uploads');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
