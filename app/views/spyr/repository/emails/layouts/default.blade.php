<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
<table width="600" border="0" cellpadding="3" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border:2px solid #000000">
	<tr valign="middle" style="background:#000000; height:35px; color:#fff">
		<td height="35" style="padding:10px;">
			<a href="{{ route('home') }}"><img src="{{asset('assets/img/logo-sm.png')}}" style="height: 50px; padding: 5px;"/></a>
		</td>
	</tr>
	<tr>
		<td style="padding:20px 10px">
			@yield('content')
		</td>
	</tr>
</table>
</body>
</html>
