@extends('spyr.repository.emails.layouts.default')

@section('content')
	<p>Hey  {{ $user->first_name }},</p>

	<p>Thank you for signing up to {{Config::get('configuration.app.name')}}!</p>

	<p>Please click <a href="{{ $activationUrl }}">here</a> to verify your account. Once verified you will be able to log in to create your profile, view and sign up to all of our current roles.</p>

	<p>In the meantime if you have any questions, feel free to drop us an email at {{Config::get('configuration.contactEmail')}}</p>

@stop
