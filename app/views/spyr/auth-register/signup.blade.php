@extends('spyr.template.auth-frame')

@section('content')
    <p class="login-box-msg">Register a new membership</p>

    <form action="{{ route('post.signup') }}" method="post">

        {{Form::token()}}
        @include('spyr.form.input-text',['var'=>['name'=>'tenant_name','label'=>'Company']])
        @include('spyr.form.input-text',['var'=>['name'=>'email','label'=>'Email']])
        @include('spyr.form.input-text',['var'=>['name'=>'email_confirm','label'=>'Confirm email']])
        @include('spyr.form.input-text',['var'=>['name'=>'name','label'=>'User name']])
        @include('spyr.form.input-text',['var'=>['name'=>'password','label'=>'Password','type'=>'password']])
        @include('spyr.form.input-text',['var'=>['name'=>'password_confirm','label'=>'Confirm password','type'=>'password']])
        @include('spyr.form.input-checkbox',['var'=>['name'=>'agree','label'=>' I agree to the <a href="#">terms</a>']])

        <div class="row">
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>

        </div>
    </form>

    {{--<div class="social-auth-links text-center">--}}
        {{--<p>- OR -</p>--}}
        {{--<a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using--}}
            {{--Facebook</a>--}}
        {{--<a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using--}}
            {{--Google+</a>--}}
    {{--</div>--}}

    <a href="login.html" class="text-center">I already have a membership</a>
@stop