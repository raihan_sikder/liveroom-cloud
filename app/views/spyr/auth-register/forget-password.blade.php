@extends('spyr.template.auth-frame')
{{-- Page title --}}
@section('content-top')

    @parent
@stop

{{-- Page content --}}
@section('content')
    <p class="login-box-msg">Forgot password</p>
    <form method="post" action="" id="forgetpassword">
        {{--CSRF Token--}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        {{--Email--}}
        <div class="form-group has-feedback {{ $errors->first('name', ' has-error') }}">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            <input type="text" name="name"  class="form-control" placeholder="Username"/>
            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
        </div>
        <!-- Form actions -->
        <button type="submit" class="btn bg-olive btn-block">Get password</button>
        <p>
            <a href="{{ route('get.signin') }}">Sign in</a>
        </p>
    </form>
@stop
@section('js')
    @parent
    <script>
        // validation by injecting validation class calling the validation engine
        $('input[name=name]').addClass('validate[required]'); // injecting class for email input field
        $("#forgetpassword").validationEngine(); // calling the engine
    </script>
@stop