@extends('spyr.template.auth-frame')

@section('content')
    <p class="login-box-msg">Sign in</p>

    <form action="{{ route('post.signin') }}" method="post">

        {{Form::token()}}

        <div class="form-group has-feedback {{ $errors->first('name', 'has-error') }}">
            <input name="name" type="text" class="form-control" placeholder="Username">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
        </div>

        <div class="form-group has-feedback {{ $errors->first('password', 'has-error') }}">
            <input name="password" type="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            {{ $errors->first('password', '<span class="help-block">:message</span>') }}
        </div>

        <div class="row">
            <div class="col-xs-4 pull-right">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Signin</button>
            </div>
        </div>
    </form>

    {{--<div class="social-auth-links text-center">--}}
        {{--<p>- OR -</p>--}}
        {{--<a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using--}}
            {{--Facebook</a>--}}
        {{--<a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using--}}
            {{--Google+</a>--}}
    {{--</div>--}}
    <a href="{{ route('forgot-password') }}">I forgot my password</a><br>
    <a href="login.html" class="text-center">I already have a membership</a>

@stop