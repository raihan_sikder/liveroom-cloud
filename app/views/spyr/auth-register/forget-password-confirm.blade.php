@extends('spyr.template.auth-frame')

{{-- Page title --}}
@section('content-top')

    @parent
@stop


@section('containerTitle')
    {{--<img src="{{asset('assets/img/logo.png')}}" style="width: 100%"/>--}}
    {{--{{configuration('app.login_screen_msg')}}--}}
@stop

{{-- Page content --}}
@section('content')

    <form method="post" action="">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <!-- New Password -->
        <div class="form-group{{ $errors->first('password', ' error') }}">
            <label class="control-label" for="password">New Password</label>
            <div class="controls">
                <input type="password" name="password" id="password" value=""/>
                    {{ $errors->first('password', '<span class="help-block">:message</span>') }}
            </div>
        </div>

        <!-- Password Confirm -->
        <div class="form-group{{ $errors->first('password_confirm', ' error') }}">
            <label class="control-label" for="password_confirm">Password Confirmation</label>

            <div class="controls">
                <input type="password" name="password_confirm" id="password_confirm" value=""/>
                {{ $errors->first('password_confirm', '<span class="help-block">:message</span>') }}
            </div>
        </div>

        <!-- Form actions -->

        <div class="footer">
            <button type="submit" class="btn bg-olive btn-block">Change password</button>
            <p>
                <a href="{{ route('get.signin') }}">Sign in</a>
            </p>
        </div>
    </form>
@stop
