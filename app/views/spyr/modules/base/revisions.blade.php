@extends('spyr.template.app-frame')
<?php
/**
 * Variables used in this view file.
 * @var $module_name string 'superheroes'
 * @var $mod Module
 * @var $superhero Superhero Object that is being edited
 * @var $element string 'superhero'
 * @var $mod Module
 * @var $revisions \Illuminate\Database\Eloquent\Collection
 */
?>
@section('sidebar-left')
    @include('spyr.modules.base.include.sidebar-left')
@stop

@section('title')
    Revisions
@stop

@section('content')
    <table class="table table-bordered table-mailbox table-condensed table-hover" id="revisionlist" >
        <thead>
        <tr>
            <th>id</th>
            <th>Change set</th>
            <th>Event</th>
            <th>Field</th>
            <th>Old</th>
            <th>New</th>
            <th>Updated By</th>
            <th>Updated At</th>
        </tr>
        </thead>
        <tbody>
            @foreach($revisions as $revision)
                <tr>
                    <td>{{$revision->id}}</td>
                    <td>{{$revision->changeset}}</td>
                    <td>{{$revision->name}}</td>
                    <td>{{$revision->field}}</td>
                    <td>{{$revision->old}}</td>
                    <td>{{$revision->new}}</td>
                    <?php $updatedby_email=User::find($revision->updated_by)->email; ?>
                    <td>{{$updatedby_email}}</td>
                    <td>{{$revision->updated_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop
@section('js')
    @parent
    <script>
        // datatable
        var table = $('#revisionlist').dataTable( {
            "bPaginate": false
        } );
    </script>
@stop

