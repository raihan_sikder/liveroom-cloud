<p>Hello User: {{ $user->email }},</p>

<p>Please click on the following link to update your password:</p>

<p><a href="{{ $forgotPasswordUrl }}">{{ $forgotPasswordUrl }}</a></p>

<p>Best regards,</p>
