<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the project.
|
*/

Route::group(['prefix' => 'api/1.0', 'before' => 'api'], function () use ($modules) {
    Route::get('customer/{code}', ['as' => 'api.get.customer', 'uses' => 'ApiController@getCustomer']);
    Route::get('test/{code}', function ($code) {
       return Input::all();
    });
});