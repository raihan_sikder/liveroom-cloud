<?php

/*
|--------------------------------------------------------------------------
| Spyr Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the spyr framework.
|
*/

Route::group(['prefix' => 'test'], function () {

    Route::get('module-tree', function () {
        User()->testFunction();
        dd();
    });

});
