<?php

/**
 * Upload
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string $uuid
 * @property string $name
 * @property string $path
 * @property string $ext
 * @property string $tags
 * @property string $desc
 * @property integer $module_id
 * @property integer $element_id
 * @property string $element_uuid
 * @property string $is_active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\Upload[] $uploads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Revision[] $revisions
 * @property-read \User $creator
 * @property-read \User $updater
 * @method static \Illuminate\Database\Query\Builder|\Upload whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereTenantId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereExt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereTags($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereModuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereElementId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereElementUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereDeletedBy($value)
 * @property integer $order
 * @property integer $uploadtype_id
 * @method static \Illuminate\Database\Query\Builder|\Upload whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUploadtypeId($value)
 */
class Upload extends Spyrmodule
{
    /**
     * Custom validation messages.
     * @var array
     */
    public static $custom_validation_messages = [
        //'name.required' => 'Custom message.',
    ];

    /**
     * Disallow from mass assignment. (Black-listed fields)
     * @var array
     */
    // protected $guarded = [];

    /**
     * Date fields
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * Mass assignment fields (White-listed fields)
     * @var array
     */
    protected $fillable = ['uuid', 'name', 'order', 'tenant_id', 'uploadtype_id', 'path', 'tags', 'desc', 'module_id', 'element_id', 'element_uuid', 'is_active', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Validation rules. For regular expression validation use array instead of pipe
     * Example: 'name' => ['required', 'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/']
     * @param Upload $element
     * @param array $merge
     * @return array
     */
    public static function rules($element, $merge = []) {
        $rules = [
            //'name'       => 'required|between:6,255|unique:uploads,name' . (isset($element->id) ? ",$element->id" : ''),
            //'name'       => 'required|between:1,255',
            'created_by' => 'integer|exists:users,id,is_active,"Yes"',
            'updated_by' => 'integer|exists:users,id,is_active,"Yes"',
            'uploadtype_id' => 'integer|exists:uploadtypes,id,is_active,"Yes"',
            'is_active' => 'in:Yes,No',
            'tenant_id' => 'integer|exists:tenants,id,is_active,"Yes"',
        ];

        return array_merge($rules, $merge);
    }
    /**
     * Automatic eager load relation by default (can be expensive)
     * @var array
     */
    // protected $with = ['relation1', 'relation2'];

    ############################################################################################
    # Model events
    ############################################################################################

    public static function boot() {
        /**
         * parent::boot() was previously used. However this invocation stops from the other classes
         * of other spyr modules(Models) to override the boot() method. Need to check more.
         * make the parent (Eloquent) boot method run.
         */
        parent::boot();
        Spyrmodule::registerObserver(get_class()); // register observer

        /************************************************************/
        // Following code block executes - when an element is in process
        // of creation for the first time but the creation has not
        // completed yet.
        /************************************************************/
        // static::creating(function (Upload $element) { });

        /************************************************************/
        // Following code block executes - after an element is created
        // for the first time.
        /************************************************************/
        // static::created(function (Upload $element) {});

        /************************************************************/
        // Following code block executes - when an already existing
        // element is in process of being updated but the update is
        // not yet complete.
        /************************************************************/
        // static::updating(function (Upload $element) {});

        /************************************************************/
        // Following code block executes - after an element
        // is successfully updated
        /************************************************************/
        //static::updated(function (Upload $element) {});

        /************************************************************/
        // Execute codes during saving (both creating and updating)
        /************************************************************/
        static::saving(function (Upload $element) {
            $valid = true;
            /************************************************************/
            // Your validation goes here
            // if($valid) $valid = $element->isSomethingDoable(true)
            /************************************************************/
            if ($valid) {
                $element->is_active = "Yes"; // Always set as 'Yes'
                $element->ext = extFrmPath($element->path); // Store file extension separately
            }
            return $valid;
        });

        /************************************************************/
        // Execute codes after model is successfully saved
        /************************************************************/
        // static::saved(function (Upload $element) { });

        /************************************************************/
        // Following code block executes - when some element is in
        // the process of being deleted. This is good place to
        // put validations for eligibility of deletion.
        /************************************************************/
        // static::deleting(function (Upload $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully deleted.
        /************************************************************/
        // static::deleted(function (Upload $element) {});

        /************************************************************/
        // Following code block executes - when an already deleted element
        // is in the process of being restored.
        /************************************************************/
        // static::restoring(function (Upload $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully restored.
        /************************************************************/
        //static::restored(function (Upload $element) {});
    }

    ############################################################################################
    # Validator functions
    ############################################################################################

    /**
     * @param bool|false $setMsgSession setting it false will not store the message in session
     * @return bool
     */
    //    public function isSomethingDoable($setMsgSession = false)
    //    {
    //        $valid = true;
    //        // Make invalid(Not request-able) if {something doesn't match with something}
    //        if ($valid && $this->id == null) {
    //            if ($setMsgSession) $valid = setError("Something is wrong. Id is Null!!"); // make valid flag false and set validation error message in session if message flag is true
    //            else $valid = false; // don't set any message only set validation as false.
    //        }
    //
    //        return $valid;
    //    }

    ############################################################################################
    # Helper functions
    ############################################################################################
    /**
     * Non static functions can be directly called $element->function();
     * Such functions are useful when an object(element) is already instantiated
     * and some processing is required for that
     */
    // public function someAction() { }

    /**
     * Static cuntions needs to be called using Model::function($id)
     * Inside static function you may need to query and get the element
     * @param $id
     */
    // public static function someOtherAction($id) { }

    /**
     * get all uploads under a module
     *
     * @param array $entry_uuid
     * @param string $filter
     * @return mixed
     */
    public static function getList($entry_uuid, $filter = '') // TODO : filter logic incomplete
    {
        $uploads = Upload::where('element_uuid', $entry_uuid);

        return $uploads->orderBy('created_at', 'DESC')->get();
    }

    /**
     * During creation of a module entry there is no id but still files can be uploaded.
     * At this time system creates an uuid and stores files against that uuid.
     * Once the creation is successful
     * @param $element_id
     * @param $element_uuid
     */
    public static function linkTemporaryUploads($element_id, $element_uuid) {
        Upload::where('element_uuid', $element_uuid)->update([
            'element_id' => $element_id,
        ]);
    }

    /**
     * Get file extension of an upload from the stored path
     * @return mixed
     */
    // Note : This is an older function that is used befor having 'ext' field in table
    //    public function ext()
    //    {
    //        $path_parts = pathinfo($this->path);
    //        $ext = $path_parts['extension'];
    //        return $ext;
    //    }

    /**
     * returns the absolute server path.
     * This function is useful for plugins that needs the file location in the operating system
     * @return string
     */
    public function absPath() {
        return public_path() . $this->path;
    }

    /**
     * Get the url for thumbnail of an upload.
     * @return string
     */
    public function thumbSrc() {

        if ($this->isImage())
            $src = route('get.download', $this->uuid);
        else
            $src = $this->extIconPath();

        return $src;
    }

    /**
     * Checks if an upload file is image
     * @return mixed
     */
    public function isImage() {
        if (imageExtension($this->ext)) {
            return true;
        }
        return false;
    }

    /**
     * 'assets/file_type_icons' contains number of file type icons.
     * @return string
     */
    public function extIconPath() {
        $ext = strtolower($this->ext); // get full lower case extension
        $icon_path = 'assets/file_type_icons/' . $ext . '.png';

        if (!File::exists($icon_path)) {
            $icon_path = 'assets/file_type_icons/noimage.png';
        }
        return asset($icon_path);
    }

    /**
     * Generate masked and plain url of the uploaded file.
     * @param bool $auth set false to generate plain url.
     * @return string
     */
    public function downloadUrl($auth = true) {
        if ($auth) return route('get.download', $this->uuid);
        return asset(str_replace(' ','%20',$this->path));
    }

    ############################################################################################
    # Permission functions
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write the functions that resolve permission to a specific model.
     * In the parent class there are the follow functions that checks whether a user has
     * permission to perform the following on an element. Rewrite these functions
     * in case more customized access management is required.
     */
    ############################################################################################

    /**
     * Checks if the $upload is viewable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isViewable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementViewable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $upload is editable by current or any user passed as parameter.
     * spyrElementEditable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isEditable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementEditable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $upload is deletable by current or any user passed as parameter.
     * spyrElementDeletable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isDeletable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementDeletable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $upload is restorable by current or any user passed as parameter.
     * spyrElementRestorable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isRestorable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementRestorable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    ############################################################################################
    # Query scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
       public function scopePopular($query) { return $query->where('votes', '>', 100); }
       public function scopeWomen($query) { return $query->whereGender('W'); }
       # Example of user
       $users = User::popular()->women()->orderBy('created_at')->get();
    */
    ############################################################################################

    // Write new query scopes here.

    ############################################################################################
    # Dynamic scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopeOfType($query, $type) { return $query->whereType($type); }
    # Example of user
    $users = User::ofType('member')->get();
    */
    ############################################################################################

    // Write new dynamic query scopes here.

    ############################################################################################
    # Model relationships
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write model relationships. In the parent class there are
     * In the Parent class there are the follow two relations creator(), updater() are
     * already defined.
     */
    ############################################################################################

    # Default relationships already available in base Class 'Spyrmodule'
    //public function updater() { returSn $this->belongsTo('User', 'updated_by'); }
    //public function creator() { return $this->belongsTo('User', 'created_by'); }

    // Write new relationships below this line

    ############################################################################################
    # Accessors & Mutators
    # ---------------------------------------------------------------------------------------- #
    /*
     * Eloquent provides a convenient way to transform your model attributes when getting or setting them. Simply
     * define a getFooAttribute method on your model to declare an accessor. Keep in mind that the methods
     * should follow camel-casing, even though your database columns are snake-case:
     */
    // Example
    // public function getFirstNameAttribute($value) { return ucfirst($value); }
    // public function setFirstNameAttribute($value) { $this->attributes['first_name'] = strtolower($value); }
    ############################################################################################

    // Write accessors and mutators here.
}
