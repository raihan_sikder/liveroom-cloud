<?php

/**
 * Revision
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string $uuid
 * @property string $changeset
 * @property string $name
 * @property integer $module_id
 * @property string $module_name
 * @property integer $element_id
 * @property string $element_uuid
 * @property string $field
 * @property string $old
 * @property string $new
 * @property string $desc
 * @property string $is_active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $deleted_by
 * @property-read \User $updater
 * @property-read \User $creator
 * @method static \Illuminate\Database\Query\Builder|\Revision whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereTenantId($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereChangeset($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereModuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereModuleName($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereElementId($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereElementUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereField($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereOld($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereNew($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereDeletedBy($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Upload[] $uploads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Revision[] $revisions
 */
class Revision extends Spyrmodule
{
    /**
     * Custom validation messages.
     * @var array
     */
    public static $custom_validation_messages = [
        //'name.required' => 'Custom message.',
    ];

    /**
     * Disallow from mass assignment. (Black-listed fields)
     * @var array
     */
    // protected $guarded = [];

    /**
     * Date fields
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * Mass assignment fields (White-listed fields)
     * @var array
     */
    protected $fillable = ['uuid', 'name', 'changeset', 'tenant_id', 'module_id', 'module_name', 'element_id', 'element_uuid', 'field', 'old', 'new', 'desc', 'is_active', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Validation rules. For regular expression validation use array instead of pipe
     * Example: 'name' => ['required', 'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/']
     * @param Revision $element
     * @param array $merge
     * @return array
     */
    public static function rules($element, $merge = [])
    {
        $rules = [
            'name' => 'required|between:1,255|unique:revisions,name,'. (isset($element->id) ? "$element->id" : 'null') . ',id,deleted_at,NULL',
            'created_by' => 'integer|exists:users,id,is_active,"Yes"',
            'updated_by' => 'integer|exists:users,id,is_active,"Yes"',
            'is_active' => 'required|in:Yes,No',
            'tenant_id' => 'tenants,id,is_active,"Yes"',
            'module_id' => 'modules,id,is_active,"Yes"',
        ];
        return array_merge($rules, $merge);
    }
    /**
     * Automatic eager load relation by default (can be expensive)
     * @var array
     */
    // protected $with = ['relation1', 'relation2'];

    ############################################################################################
    # Model events
    ############################################################################################

    public static function boot()
    {
        /**
         * parent::boot() was previously used. However this invocation stops from the other classes
         * of other spyr modules(Models) to override the boot() method. Need to check more.
         * make the parent (Eloquent) boot method run.
         */
        parent::boot();
        Spyrmodule::registerObserver(get_class()); // register observer

        /************************************************************/
        // Following code block executes - when an element is in process
        // of creation for the first time but the creation has not
        // completed yet.
        /************************************************************/
        // static::creating(function (Revision $element) { });

        /************************************************************/
        // Following code block executes - after an element is created
        // for the first time.
        /************************************************************/
        // static::created(function (Revision $element) {});

        /************************************************************/
        // Following code block executes - when an already existing
        // element is in process of being updated but the update is
        // not yet complete.
        /************************************************************/
        // static::updating(function (Revision $element) {});

        /************************************************************/
        // Following code block executes - after an element
        // is successfully updated
        /************************************************************/
        //static::updated(function (Revision $element) {});

        /************************************************************/
        // Execute codes during saving (both creating and updating)
        /************************************************************/
        static::saving(function (Revision $element) {

            $valid = true;
            /************************************************************/
            // Your validation goes here
            // if($valid) $valid = $element->isSomethingDoable(true)
            /************************************************************/
            // fill common fields, null-fill, trim blanks from input
            $element->is_active = 'Yes';

            return $valid;
        });

        /************************************************************/
        // Execute codes after model is successfully saved
        /************************************************************/
        // static::saved(function (Revision $element) {});

        /************************************************************/
        // Following code block executes - when some element is in
        // the process of being deleted. This is good place to
        // put validations for eligibility of deletion.
        /************************************************************/
        // static::deleting(function (Revision $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully deleted.
        /************************************************************/
        // static::deleted(function (Revision $element) {});

        /************************************************************/
        // Following code block executes - when an already deleted element
        // is in the process of being restored.
        /************************************************************/
        // static::restoring(function (Revision $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully restored.
        /************************************************************/
        //static::restored(function (Revision $element) {});
    }

    ############################################################################################
    # Validator functions
    ############################################################################################

    /**
     * @param bool|false $setMsgSession setting it false will not store the message in session
     * @return bool
     */
    //    public function isSomethingDoable($setMsgSession = false)
    //    {
    //        $valid = true;
    //        // Make invalid(Not request-able) if {something doesn't match with something}
    //        if ($valid && $this->id == null) {
    //            if ($setMsgSession) $valid = setError("Something is wrong. Id is Null!!"); // make valid flag false and set validation error message in session if message flag is true
    //            else $valid = false; // don't set any message only set validation as false.
    //        }
    //
    //        return $valid;
    //    }

    ############################################################################################
    # Helper functions
    ############################################################################################
    /**
     * Non static functions can be directly called $element->function();
     * Such functions are useful when an object(element) is already instantiated
     * and some processing is required for that
     */
    // public function someAction() { }

    /**
     * Static functions needs to be called using Model::function($id)
     * Inside static function you may need to query and get the element
     * @param $id
     */
    // public static function someOtherAction($id) { }


    /**
     * Get changes of a model and store in session.
     * @param \Eloquent $element
     */
    public static function keepChangesInSession(Eloquent $element)
    {
        Session::put('model-changes', Revision::getChanges($element));
    }

    /**
     * Get the changes in an array
     * @param Eloquent $filled_element
     * @param array $except
     * @return array
     */
    public static function getChanges(Eloquent $filled_element, $except = ['updated_at'])
    {
        $changes = [];
        if (isset($filled_element->id)) {
            $new_values = $filled_element->getDirty();
            $Model = get_class($filled_element);
            $original = $Model::find($filled_element->id);

            $i = 0;
            foreach ($new_values as $attribute => $value) {
                if (!in_array($attribute, $except) && $original->$attribute != $value) {

                    $old_value = $original->$attribute;
                    if (is_array($old_value)) {
                        $old_value = json_encode($old_value);
                    }

                    $new_value = $value;
                    if (is_array($new_value)) {
                        $new_value = json_encode($new_value);
                    }

                    $changes[$i] = [
                        "field" => $attribute,
                        "old" => $old_value,
                        "new" => $new_value,
                    ];
                    $i++;
                }
            }
            return $changes;
        }
    }

    /**
     * Fetch changes that are stored in session and save in database.
     * @param string $change_name
     * @param \Eloquent $element
     * @param string $desc
     */
    public static function storeChangesFromSession($change_name = "", Eloquent $element, $desc = "")
    {
        if (Session::has('model-changes')) {
            Revision::storeChanges($change_name, $element, Session::get('model-changes'), $desc);
        }
    }

    /**
     * @param string $change_name : assign a meaningful name of the change
     * @param Eloquent $element
     * @param array $changes
     * @param string $desc
     * @internal param array $change_items
     */
    public static function storeChanges($change_name = '', Eloquent $element, $changes = [], $desc = "")
    {
        if (isset($element->id) && count($changes)) {
            $changeset = str_random(8);
            if ($module = Module::whereName(moduleName(get_class($element)))->first()) {
                foreach ($changes as $change) {
                    if (is_array($change['new'])) {
                        $change['new'] = implode(',', $change['new']);
                    }
                    if (is_array($change['old'])) {
                        $change['old'] = implode(',', $change['old']);
                    }

                    $change = Revision::create([
                        "name" => $change_name,
                        "changeset" => $changeset,
                        "module_id" => $module->id,
                        "module_name" => $module->name,
                        "element_id" => $element->id,
                        "element_uuid" => $element->uuid,
                        "field" => $change['field'],
                        "old" => $change['old'],
                        "new" => $change['new'],
                        "desc" => $desc,
                        "created_by" => $element->updated_by,
                        "updated_by" => $element->updated_by,
                    ]);
                }
            }
        }
    }

    ############################################################################################
    # Permission functions
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write the functions that resolve permission to a specific model.
     * In the parent class there are the follow functions that checks whether a user has
     * permission to perform the following on an element. Rewrite these functions
     * in case more customized access management is required.
     */
    ############################################################################################

    /**
     * Checks if the $revision is viewable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isViewable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementViewable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $revision is editable by current or any user passed as parameter.
     * spyrElementEditable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isEditable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementEditable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $revision is deletable by current or any user passed as parameter.
     * spyrElementDeletable() is the primary default checker based on permission
     * whether this should be allowed or not. The logical can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isDeletable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementDeletable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $revision is restorable by current or any user passed as parameter.
     * spyrElementRestorable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isRestorable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementRestorable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    ############################################################################################
    # Query scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
       public function scopePopular($query) { return $query->where('votes', '>', 100); }
       public function scopeWomen($query) { return $query->whereGender('W'); }
       # Example of user
       $users = User::popular()->women()->orderBy('created_at')->get();
    */
    ############################################################################################

    // Write new query scopes here.

    ############################################################################################
    # Dynamic scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:``
     */
    /*
    public function scopeOfType($query, $type) { return $query->whereType($type); }
    # Example of user
    $users = User::ofType('member')->get();
    */
    ############################################################################################

    // Write new dynamic query scopes here.

    ############################################################################################
    # Model relationships
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write model relationships. In the parent class there are
     * In the Parent class there are the follow two relations creator(), updater() are
     * already defined.
     */
    ############################################################################################

    # Default relationships already available in base Class 'Spyrmodule'
    //public function updater() { return $this->belongsTo('User', 'updated_by'); }
    //public function creator() { return $this->belongsTo('User', 'created_by'); }

    // Write new relationships below this line

    ############################################################################################
    # Accessors & Mutators
    # ---------------------------------------------------------------------------------------- #
    /*
     * Eloquent provides a convenient way to transform your model attributes when getting or setting them. Simply
     * define a getFooAttribute method on your model to declare an accessor. Keep in mind that the methods
     * should follow camel-casing, even though your database columns are snake-case:
     */
    // Example
    // public function getFirstNameAttribute($value) { return ucfirst($value); }
    // public function setFirstNameAttribute($value) { $this->attributes['first_name'] = strtolower($value); }
    ############################################################################################

    // Write accessors and mutators here.

}
