<?php

/**
 * User
 * This model does not extend the Spyr based model. app/models/SpyrModule
 * Rather it extends a Sentry class SentryUserModel. As a result all the Spyr property of
 * the base model had to be duplicated in this class.
 *
 * @property integer $id
 * @property string $uuid
 * @property integer $tenant_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $permissions
 * @property string $group_ids_csv
 * @property string $group_titles_csv
 * @property boolean $activated
 * @property string $activation_code
 * @property \Carbon\Carbon $activated_at
 * @property \Carbon\Carbon $last_login
 * @property string $persist_code
 * @property string $reset_password_code
 * @property string $is_system_user
 * @property string $is_active
 * @property \Carbon\Carbon $created_at
 * @property integer $created_by
 * @property \Carbon\Carbon $updated_at
 * @property integer $updated_by
 * @property \Carbon\Carbon $deleted_at
 * @property integer $deleted_by
 * @property-read \User $updater
 * @property-read \User $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\static::$groupModel[] $groups
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereTenantId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePermissions($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereGroupIdsCsv($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereGroupTitlesCsv($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereActivated($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereActivationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereActivatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePersistCode($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereResetPasswordCode($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereIsSystemUser($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereDeletedBy($value)
 * @property string $is_editable_by_tenant
 * @property-read \Illuminate\Database\Eloquent\Collection|\Upload[] $uploads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Revision[] $revisions
 * @method static \Illuminate\Database\Query\Builder|\User whereIsEditableByTenant($value)
 */
class User extends Spyruser
{
    /**
     * Mass assignment fields (White-listed fields)
     * @var array
     */
    // protected $fillable = ['uuid', 'name', 'password', 'group_ids_csv', 'email', 'tenant_id', 'activated', 'is_editable_by_tenant', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Disallow from mass assignment. (Black-listed fields)
     * @var array
     */
    // protected $guarded = [];

    /**
     * Date fields
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    // Enable soft deleting.
    // use Illuminate\Database\Eloquent\SoftDeletingTrait;

    /*protected $hashableAttributes = [
        'password',
        //'persist_code',
    ];*/
    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */
    // protected $hidden = ['password', 'remember_token', 'reset_password_code', 'activation_code',];

    //protected $hashableAttributes = ['password'];

    /**
     * Validation rules. For regular expression validation use array instead of pipe
     * Example: 'name' => ['required', 'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/']
     * @param       $element
     * @param array $merge
     * @return array
     */
    /*public static function rules($element, $merge = [])
    {

        $rules = [
            'name'             => ['required', 'between:1,255', 'unique:users,name' . (isset($element->id) ? ",$element->id" : ''), 'Regex:/^[a-z0-9\-]+$/'],
            'email'            => 'required|email',
            'created_by'       => 'exists:users,id,is_active,"Yes"',
            'updated_by'       => 'exists:users,id,is_active,"Yes"',
            'password'         => 'between:3,32',
            'password_confirm' => 'between:3,32|same:password',
            //'is_active'        => 'required|in:Yes,No',
        ];
        // While creation/registration of user password and password_confirm both should be available
        // Also if one password is given the other one should be given as well
        if (!isset($element->id)) {
            $rules = array_merge($rules, [
                'password'         => 'required|between:3,32',
                'password_confirm' => 'required|between:3,32|same:password',
            ]);
        }

        return array_merge($rules, $merge);
    }*/

    /*public static $custom_validation_messages = [
        //'name.required' => 'Custom message.',
    ];*/

    /**
     * Automatic eager load relation by default (can be expensive)
     * @var array
     */
    // protected $with = ['relation1', 'relation2'];

    ############################################################################################
    # Model events
    ############################################################################################
    public static function boot()
    {
        /**
         * parent::boot() was previously used. However this invocation stops from the other classes
         * of other spyr modules(Models) to override the boot() method. Need to check more.
         * make the parent (Eloquent) boot method run.
         */
        // parent::boot();
        Spyrmodule::registerObserver(get_class()); // register observer

        /************************************************************/
        // Following code block executes - when an element is in process
        // of creation for the first time but the creation has not
        // completed yet.
        /************************************************************/
        // static::creating(function (User $element) { });

        /************************************************************/
        // Following code block executes - after an element is created
        // for the first time.
        /************************************************************/
        // static::created(function (User $element) {});

        /************************************************************/
        // Following code block executes - when an already existing
        // element is in process of being updated but the update is
        // not yet complete.
        /************************************************************/
        // static::updating(function (User $element) {});

        /************************************************************/
        // Following code block executes - after an element
        // is successfully updated
        /************************************************************/
        //static::updated(function (User $element) {});

        /************************************************************/
        // Execute codes during saving (both creating and updating)
        /************************************************************/
        static::saving(function (User $element) {
            $valid = true;
            if (!$element = Spyruser::make($element)) {
                $valid = setError('User could not be saved. Error: Spyruser::make($element)');
            }
            return $valid;
        });


        /************************************************************/
        // Execute codes after model is successfully saved
        /************************************************************/
        static::saved(function (User $element) {
            $element->updateGroups();
        });

        /************************************************************/
        // Following code block executes - when some element is in
        // the process of being deleted. This is good place to
        // put validations for eligibility of deletion.
        /************************************************************/
        // static::deleting(function (User $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully deleted.
        /************************************************************/
        // static::deleted(function (User $element) {});

        /************************************************************/
        // Following code block executes - when an already deleted element
        // is in the process of being restored.
        /************************************************************/
        // static::restoring(function (User $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully restored.
        /************************************************************/
        //static::restored(function (User $element) {});
    }

    ############################################################################################
    # Validator functions
    ############################################################################################

    /**
     * @param bool|false $setMsgSession setting it false will not store the message in session
     * @return bool
     */
    //    public function isSomethingDoable($setMsgSession = false)
    //    {
    //        $valid = true;
    //        // Make invalid(Not request-able) if {something doesn't match with something}
    //        if ($valid && $this->id == null) {
    //            if ($setMsgSession) $valid = setError("Something is wrong. Id is Null!!"); // make valid flag false and set validation error message in session if message flag is true
    //            else $valid = false; // don't set any message only set validation as false.
    //        }
    //
    //        return $valid;
    //    }

    ############################################################################################
    # Helper functions
    ############################################################################################
    /**
     * Non static functions can be directly called $element->function();
     * Such functions are useful when an object(element) is already instantiated
     * and some processing is required for that
     */
    // public function someAction() { }

    /**
     * Static functions needs to be called using Model::function($id)
     * Inside static function you may need to query and get the element
     *
     * @param $id
     */
    // public static function someOtherAction($id) { }

    ############################################################################################
    # Permission functions
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write the functions that resolve permission to a specific model.
     * In the Parent class there are the follow functions that checks whether a user has
     * permission to perform the following on an element. Rewrite these functions
     * in case more customized access management is required.
     */
    ############################################################################################

    /**
     * Checks if the $element is creatable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    /*
    public function isCreatable($user_id = null, $set_msg = false)
    {
        if ($valid = spyrElementCreatable($this, $user_id, $set_msg)) {
            // Write new validation logic for this operation in this block.
        }
        return $valid;
    }
    */
    /**
     * Checks if the $superhero is viewable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    /*public function isViewable($user_id = null, $set_msg = false)
    {
        $valid = false;
        if ($valid = spyrElementViewable($this, $user_id, $set_msg)) {
            // Write new validation logic for this operation in this block.
        }
        return $valid;
    }*/

    /**
     * Checks if the $superhero is editable by current or any user passed as parameter.
     * spyrElementEditable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    /*public function isEditable($user_id = null, $set_msg = false)
    {
        $valid = false;
        if ($valid = spyrElementEditable($this, $user_id, $set_msg)) {
            // Write new validation logic for this operation in this block.
        }
        return $valid;
    }*/

    /**
     * Checks if the $superhero is deletable by current or any user passed as parameter.
     * spyrElementDeletable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    /*public function isDeletable($user_id = null, $set_msg = false)
    {
        $valid = false;
        if ($valid = spyrElementDeletable($this, $user_id, $set_msg)) {
            // Write new validation logic for this operation in this block.
        }
        return $valid;
    }*/

    /**
     * Checks if the $superhero is restorable by current or any user passed as parameter.
     *  spyrElementRestorable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @return bool
     * @internal param null $user_id
     * @internal param bool $set_msg
     */
    /*public function isRestorable($user_id = null, $set_msg = false)
    {
        $valid = false;
        if ($valid = spyrElementRestorable($this, $user_id, $set_msg)) {
            // Write new validation logic for this operation in this block.
        }
        return $valid;
    }*/

    ############################################################################################
    # Query scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopePopular($query) { return $query->where('votes', '>', 100); }
    public function scopeWomen($query) { return $query->whereGender('W'); }
    # Example of user
    $users = User::popular()->women()->orderBy('created_at')->get();
    */
    ############################################################################################

    // Write new query scopes here.

    ############################################################################################
    # Dynamic scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopeOfType($query, $type) { return $query->whereType($type); }
    # Example of user
    $users = User::ofType('member')->get();
    */
    ############################################################################################

    // Write new dynamic query scopes here.

    ############################################################################################
    # Model relationships
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write model relationships. In the parent class there are
     * In the parent class there are the follow two relations creator(), updater() are
     * already defined.
     */
    ############################################################################################


    /*public function updater()
    {
        return $this->belongsTo('User', 'updated_by');
    }

    public function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }*/

    // Write new relationships below this line

    ############################################################################################
    # Accessors & Mutators
    # ---------------------------------------------------------------------------------------- #
    /*
     * Eloquent provides a convenient way to transform your model attributes when getting or setting them. Simply
     * define a getFooAttribute method on your model to declare an accessor. Keep in mind that the methods
     * should follow camel-casing, even though your database columns are snake-case:
     */
    // Example
    // public function getFirstNameAttribute($value) { return ucfirst($value); }
    // public function setFirstNameAttribute($value) { $this->attributes['first_name'] = strtolower($value); }
    ############################################################################################

    // Write accessors and mutators here.
}
