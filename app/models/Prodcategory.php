<?php

/**
 * Prodcategory
 * For eloquent features check the documentation https://laravel.com/docs/4.2/eloquent
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Upload[] $uploads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Revision[] $revisions
 * @property-read \User $updater
 * @property-read \User $creator
 * @property integer $id
 * @property integer $tenant_id
 * @property string $uuid
 * @property string $name
 * @property integer $order
 * @property integer $parent_id
 * @property string $desc
 * @property string $is_active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $deleted_by
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereTenantId($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Prodcategory whereDeletedBy($value)
 */
class Prodcategory extends Spyrmodule
{
    /**
     * Custom validation messages.
     * @var array
     */
    public static $custom_validation_messages = [
        //'name.required' => 'Custom message.',
    ];
    /**
     * Disallow from mass assignment. (Black-listed fields)
     * @var array
     */
    // protected $guarded = [];

    /**
     * Date fields
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * Mass assignment fields (White-listed fields)
     * @var array
     */
    protected $fillable = ['uuid', 'name', 'tenant_id', 'order', 'parent_id', 'desc', 'is_active', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Validation rules. For regular expression validation use array instead of pipe
     * Example: 'name' => ['required', 'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/']
     * @param       $element
     * @param array $merge
     * @return array
     */
    public static function rules($element, $merge = []) {
        $rules = [
            //'name' => 'required|between:1,255|unique:prodcategories,name' . (isset($element->id) ? ",$element->id" : ''),
            'name' => 'required|between:1,255',
            'parent_id' => 'integer|exists:prodcategories,id,is_active,"Yes"',
            'created_by' => 'integer|exists:users,id,is_active,"Yes"',
            'updated_by' => 'integer|exists:users,id,is_active,"Yes"',
            'is_active' => 'required|in:Yes,No',
            //'tenant_id' => 'required|tenants,id,is_active,"Yes"',
        ];
        return array_merge($rules, $merge);
    }
    /**
     * Automatic eager load relation by default (can be expensive)
     * @var array
     */
    // protected $with = ['relation1', 'relation2'];

    ############################################################################################
    # Model events
    ############################################################################################

    public static function boot() {
        /**
         * parent::boot() was previously used. However this invocation stops from the other classes
         * of other spyr modules(Models) to override the boot() method. Need to check more.
         * make the parent (Eloquent) boot method run.
         */
        parent::boot();
        Spyrmodule::registerObserver(get_class()); // register observer

        /************************************************************/
        // Following code block executes - when an element is in process
        // of creation for the first time but the creation has not
        // completed yet.
        /************************************************************/
        // static::creating(function (Prodcategory $element) { });

        /************************************************************/
        // Following code block executes - after an element is created
        // for the first time.
        /************************************************************/
        // static::created(function (Prodcategory $element) {});

        /************************************************************/
        // Following code block executes - when an already existing
        // element is in process of being updated but the update is
        // not yet complete.
        /************************************************************/
        // static::updating(function (Prodcategory $element) {});

        /************************************************************/
        // Following code block executes - after an element
        // is successfully updated
        /************************************************************/
        //static::updated(function (Prodcategory $element) {});

        /************************************************************/
        // Execute codes during saving (both creating and updating)
        /************************************************************/
        //        static::saving(function (Prodcategory $element) {
        //            $valid = true;
        //            /************************************************************/
        //            // Your validation goes here
        //            // if($valid) $valid = $element->isSomethingDoable(true)
        //            /************************************************************/
        //            return $valid;
        //        });

        /************************************************************/
        // Execute codes after model is successfully saved
        /************************************************************/
        // static::saved(function (Prodcategory $element) {});

        /************************************************************/
        // Following code block executes - when some element is in
        // the process of being deleted. This is good place to
        // put validations for eligibility of deletion.
        /************************************************************/
        // static::deleting(function (Prodcategory $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully deleted.
        /************************************************************/
        // static::deleted(function (Prodcategory $element) {});

        /************************************************************/
        // Following code block executes - when an already deleted element
        // is in the process of being restored.
        /************************************************************/
        // static::restoring(function (Prodcategory $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully restored.
        /************************************************************/
        //static::restored(function (Prodcategory $element) {});
    }

    ############################################################################################
    # Validator functions
    ############################################################################################

    /**
     * @param bool|false $set_msg setting it false will not store the message in session
     * @return bool
     */
    //    public function isSomethingDoable($set_msg = false)
    //    {
    //        $valid = true;
    //        // Make invalid(Not request-able) if {something doesn't match with something}
    //        if ($valid && $this->id == null) {
    //            $valid = setError("Something is wrong. Id is Null!!",$set_msg); // make valid flag false and set validation error message in session if message flag is true
    //        }
    //        return $valid;
    //    }

    ############################################################################################
    # Helper functions
    ############################################################################################
    /**
     * Non static functions can be directly called $element->function();
     * Such functions are useful when an object(element) is already instantiated
     * and some processing is required for that
     */
    // public function someAction() { }

    /**
     * Static cuntions needs to be called using Model::function($id)
     * Inside static function you may need to query and get the element
     * @param $id
     */
    // public static function someOtherAction($id) { }

    public static function getCategoryTreeForApi($tenant_id, $parent_id = null) {

        $prodcategories = Prodcategory::where('tenant_id', $tenant_id)->where('parent_id', $parent_id)->whereIsActive('Yes')->orderBy('order', 'ASC')->get();

        $data = [];

        foreach ($prodcategories as $prodcategory) {

            // Get category thumbnail
            $category_thumbnail = '#';
            if ($upload = Upload::where('element_id', $prodcategory->id)->where('module_id', $prodcategory->module()->id)->where('uploadtype_id', 2)->whereIsActive('Yes')->orderBy('order', 'ASC')->first()) {
                $category_thumbnail = $upload->downloadUrl(false);
            }

            // Get category thumbnail
            $category_three_D_files = [];
            if ($uploads = Upload::where('element_id', $prodcategory->id)->where('module_id', $prodcategory->module()->id)->where('uploadtype_id', 4)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                foreach ($uploads as $upload) {
                    $category_three_D_files[] = $upload->downloadUrl(false);
                }
            }

            // Get category products
            $category_products = [];
            if ($products = Product::where('prodcategory_id', $prodcategory->id)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                foreach ($products as $product) {

                    // Get category thumbnail
                    $product_thumbnail = '#';
                    if ($upload = Upload::where('element_id', $product->id)->where('module_id', $product->module()->id)->where('uploadtype_id', 3)->whereIsActive('Yes')->orderBy('order', 'ASC')->first()) {
                        $product_thumbnail = $upload->downloadUrl(false);
                    }

                    // Get category thumbnail
                    $product_three_D_files = [];
                    if ($uploads = Upload::where('element_id', $product->id)->where('module_id', $product->module()->id)->where('uploadtype_id', 4)->whereIsActive('Yes')->orderBy('order', 'ASC')->get()) {
                        foreach ($uploads as $upload) {
                            $product_three_D_files[] = $upload->downloadUrl(false);
                        }
                    }

                    $category_products[] = [
                        'id' => $product->id,
                        'uuid' => $product->uuid,
                        'name' => $product->name,
                        'code' => $product->code,
                        'order' => (int)$product->order,
                        'price' => (float)$product->price,
                        'desc' => $product->desc,
                        'prodcategory_id' => $product->prodcategory_id,
                        'placement' => $product->placement,
                        'default_height' => $product->default_height,
                        'buy_now_url' => $product->buy_now_url,
                        'rating' => (int)$product->rating,
                        'color_variants' => json_decode($product->color_variants),
                        'product_thumbnail' => $product_thumbnail,
                        'product_3d_files' => $product_three_D_files,
                    ];
                }
            }

            $data[] = [
                'id' => $prodcategory->id,
                'uuid' => $prodcategory->uuid,
                'name' => $prodcategory->name,
                'order' => $prodcategory->order,
                'parent_id' => $prodcategory->parent_id,
                'desc' => $prodcategory->desc,
                'category_thumbnail' => $category_thumbnail,
                'category_3d_files' => $category_three_D_files,
                'products' => $category_products,
                'prodcategories' => Prodcategory::getCategoryTreeForApi($tenant_id, $prodcategory->id)
            ];
        }

        return $data;

    }


    ############################################################################################
    # Permission functions
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write the functions that resolve permission to a specific model.
     * In the parent class there are the follow functions that checks whether a user has
     * permission to perform the following on an element. Rewrite these functions
     * in case more customized access management is required.
     */
    ############################################################################################

    /**
     * Checks if the $prodcategory is viewable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isViewable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementViewable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $prodcategory is editable by current or any user passed as parameter.
     * spyrElementEditable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isEditable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementEditable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $prodcategory is deletable by current or any user passed as parameter.
     * spyrElementDeletable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isDeletable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementDeletable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $prodcategory is restorable by current or any user passed as parameter.
     * spyrElementRestorable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isRestorable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementRestorable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    ############################################################################################
    # Query scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
       public function scopePopular($query) { return $query->where('votes', '>', 100); }
       public function scopeWomen($query) { return $query->whereGender('W'); }
       # Example of user
       $users = User::popular()->women()->orderBy('created_at')->get();
    */
    ############################################################################################

    // Write new query scopes here.

    ############################################################################################
    # Dynamic scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopeOfType($query, $type) { return $query->whereType($type); }
    # Example of user
    $users = User::ofType('member')->get();
    */
    ############################################################################################

    // Write new dynamic query scopes here.

    ############################################################################################
    # Model relationships
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write model relationships. In the parent class there are
     * In the parent class there are the follow two relations creator(), updater() are
     * already defined.
     */
    ############################################################################################

    # Default relationships already available in base Class 'Spyrmodule'
    //public function updater() { return $this->belongsTo('User', 'updated_by'); }
    //public function creator() { return $this->belongsTo('User', 'created_by'); }

    public function prodcategories() { return $this->hasMany('Prodcategory', 'parent_id'); }

    // Write new relationships below this line

    ############################################################################################
    # Accessors & Mutators
    # ---------------------------------------------------------------------------------------- #
    /*
     * Eloquent provides a convenient way to transform your model attributes when getting or setting them. Simply
     * define a getFooAttribute method on your model to declare an accessor. Keep in mind that the methods
     * should follow camel-casing, even though your database columns are snake-case:
     */
    // Example
    // public function getFirstNameAttribute($value) { return ucfirst($value); }
    // public function setFirstNameAttribute($value) { $this->attributes['first_name'] = strtolower($value); }
    ############################################################################################

    // Write accessors and mutators here.

}
