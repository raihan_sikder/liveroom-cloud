// enable validation
/**
 *    This function enables the js to run front-end validation.
 */
function enableValidation(form_name) {


    // 1. get id of button that is pressed for action trigger
    var btnId = $('form[name=' + form_name + '] button[type=submit]').attr('id');

    // 1.1 Change the type from submit to button so that user cannot submit form but still click the button
    $('form[name=' + form_name + '] button[id=' + btnId + ']').attr('type', 'button');

    // 2. enable json return type
    $('form[name=' + form_name + '] input[name=ret]').val('json');

    // 3. instantiate validationEngine with some options
    $('form[name=' + form_name + ']').validationEngine({prettySelect: true, promptPosition: "topLeft", scroll: true});

    // 4. Run validation on submit button click. If all frontend validatoins are ok then only ajax validation will execute
    $('form[name=' + form_name + '] button[id=' + btnId + ']').click(function () {

        $('.collapse').collapse('show');

        var btnText = $(this).html();
        $(this).html('Working...');
        $(this).addClass('disabled'); // change button text during ajax call


        if ($('form[name=' + form_name + ']').validationEngine('validate')) {
            $.ajax({
                datatype: 'json',
                method: "POST",
                url: $('form[name=' + form_name + ']').attr('action'),
                data: $('form[name=' + form_name + ']').serialize()
            }).done(function (ret) {

                ret = parseJson(ret);

                if (ret.status == 'fail') {
                    showValidationAlert(ret, false);
                } else if (ret.status == 'success') {
                    $('form[name=' + form_name + ']').validationEngine('hideAll');
                }

                loadMsg(ret);
                $('#msgModal').modal('show');

                if (ret.status == 'success' && ret.redirect.length > 0) {
                    window.location.replace(ret.redirect);
                }

                $('form[name=' + form_name + '] button[id=' + btnId + ']').html(btnText).removeClass('disabled');
            });
        } else {
            $(this).html(btnText).removeClass('disabled');
        }
    });
}

/***********************************************************
 *  This funciton is spawned from the original enableValidation.
 *  In addition to the original function this one can take a success handler function
 *  that can trigger custom actions upon success
 **********************************************************/
function enableValidationWithSuccssHandler(form_name, handler) {

    // 1. get id of button that is pressed for action trigger
    var btnId = $('form[name=' + form_name + '] button[type=submit]').attr('id');

    // 1.1 Change the type from submit to button so that user cannot submit form but still click the button
    $('form[name=' + form_name + '] button[id=' + btnId + ']').attr('type', 'button');

    // 2. enable json return type
    $('form[name=' + form_name + '] input[name=ret]').val('json');

    // 3. instantiate validationEngine with some options
    $('form[name=' + form_name + ']').validationEngine({prettySelect: true, promptPosition: "topLeft", scroll: true});

    // 4. Run validation on submit button click. If all frontend validatoins are ok then only ajax validation will execute
    $('form[name=' + form_name + '] button[id=' + btnId + ']').click(function () {

        var btnText = $(this).html();
        $(this).html('Working...');
        $(this).addClass('disabled'); // change button text during ajax call

        if ($('form[name=' + form_name + ']').validationEngine('validate')) {
            $.ajax({
                datatype: 'json',
                method: "POST",
                url: $('form[name=' + form_name + ']').attr('action'),
                data: $('form[name=' + form_name + ']').serialize()
            }).done(function (ret) {
                ret = parseJson(ret);
                if (ret.status == 'fail') {
                    showValidationAlert(ret, false);
                } else if (ret.status == 'success') {
                    $('form[name=' + form_name + ']').validationEngine('hideAll');
                }

                loadMsg(ret);
                $('#msgModal').modal('show');

                if (ret.status == 'success') {
                    handler(ret);
                }

                $('form[name=' + form_name + '] button[id=' + btnId + ']').html(btnText).removeClass('disabled');
            });
        } else {
            $(this).html(btnText).removeClass('disabled');
        }
    });
}

// show validation red boxes against each field
function showValidationAlert(ret, showAlert) {

    var str = '';
    $.each(ret.validation_errors, function (k, v) {
        str += "\n" + k + ": " + v;
        $("#" + k).validationEngine('showPrompt', v, 'error');
    });

    if (showAlert) {
        alert(ret.status + " - " + ret.message + "\n" + str);
    }

}
/*
 *  loadMsg clears and loads all new error, message and success note in the  modal that shows just after ajax submit.
 */
function loadMsg(ret) {
    $('.ajax-msg').empty().hide(); // first hide all blocks

    var hasError = false;
    var hasSuccess = false;
    var hasMessage = false;
    if (ret.status == 'fail') {
        hasError = true;
        $('div#msgError').append('<h4>Error : ' + ret.message + '</h4>');
        $.each(ret.validation_errors, function (k, v) {
            if (v.length) {
                $('div#msgError').append(k + ': ' + v + '<br/>');

            }
        });
        $.each(ret.session_error, function (k, v) {
            if (v.length) {
                $('div#msgError').append(v + '<br/>');

            }
        });
    } else if (ret.status == 'success') {
        hasSuccess = true;
        $('div#msgSuccess').append('<h4>Success</h4>');
        $.each(ret.session_success, function (k, v) {
            if (v.length) {
                $('div#msgSuccess').append(v + '<br/>');
            }
        });
    }

    $('div#msgMessage').append('<h4>Message</h4>');
    $.each(ret.session_message, function (k, v) {
        if (v.length) {
            $('div#msgMessage').append(v + '<br/>');
            hasMessage = true;
        }
    });

    if (hasError) {
        $('div#msgError').show()
    }
    if (hasSuccess) {
        $('div#msgSuccess').show()
    }
    if (hasMessage) {
        $('div#msgMessage').show()
    }
}

